# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = aux
QT -= gui

OTHER_FILES += triflevpn/*.qml triflevpn/*.js

qml_pages.path = /usr/share/sailfish-vpn/
qml_pages.files = triflevpn

INSTALLS += qml_pages
