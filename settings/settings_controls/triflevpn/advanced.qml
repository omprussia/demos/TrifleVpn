// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0

Column {
    function setProperties(providerProperties) {
        dummyPort.text = getProperty('dummyvpn.Port')
        dummyTrustedCert.text = getProperty('dummyvpn.TrustedCert')
        dummyBlockIPv6.setValue(getProperty('dummyvpn.BlockIPv6'))
        dummyAppendAdditionalGateways.setValue(getProperty('dummyvpn.AppendAdditionalGateways'))
    }

    function updateProperties(providerProperties) {
        updateProvider('dummyvpn.Port', dummyPort.text)
        updateProvider('dummyvpn.TrustedCert', dummyTrustedCert.text)
        updateProvider('dummyvpn.BlockIPv6', dummyBlockIPv6.selection)
        updateProvider('dummyvpn.AppendAdditionalGateways', dummyAppendAdditionalGateways.selection)
    }

    width: parent.width

    SectionHeader {
        //% "Server options"
        text: qsTrId("trifle_vpn-server-options")
    }

    ConfigTextField {
        id: dummyPort

        //% "Server port"
        label: qsTrId("trifle_vpn-server-port")
        inputMethodHints: Qt.ImhDigitsOnly
    }

    ConfigTextField {
        id: dummyTrustedCert

        //% "Trusted certificate fingerprint"
        label: qsTrId("trifle_vpn-trusted-cert-fingerprint")
    }

    SectionHeader {
        //% "Security options"
        text: qsTrId("trifle_vpn-security-options")
    }

    ConfigComboBox {
        id: dummyBlockIPv6

        values: ["_default", "true", "false"]

        //% "Disable IPv6"
        label: qsTrId("trifle_vpn-security-block-ipv6")
    }

    ConfigComboBox {
        id: dummyAppendAdditionalGateways

        values: ["_default", "true", "false"]

        //% "Append additional gateways"
        label: qsTrId("trifle_vpn-append-additional-gateways")
    }
}
