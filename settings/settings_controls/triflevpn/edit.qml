// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0
import ru.auroraos.TrifleVPNSystemSettings 1.0

VpnPlatformEditDialog {
    vpnType: "triflevpn"
    title: newConnection
             //% "Trifle add connection"
           ? qsTrId("trifle_vpn-connection-add")
             //% "Trifle edit connection"
           : qsTrId("trifle_vpn-connection-edit")

    Component.onCompleted: init()
    onAccepted: saveConnection()
}
