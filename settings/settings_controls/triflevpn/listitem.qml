// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import Sailfish.Settings.Networking.Vpn 1.0
import ru.auroraos.TrifleVPNSystemSettings 1.0

VpnTypeItem {
    //% "Trifle list item name"
    name: qsTrId("trifle_vpn-type-name")
    //% "Trifle list item description"
    description: qsTrId("trifle_vpn-type-description")
}
