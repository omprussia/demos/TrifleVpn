// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QtGlobal>
#include <QtQml>

#include "stubitem.h"

class TrifleVpnTranslator : public QTranslator
{
    Q_OBJECT
public:
    TrifleVpnTranslator(QObject *parent = nullptr)
        : QTranslator(parent)
    {
        qApp->installTranslator(this);
    }

    virtual ~TrifleVpnTranslator()
    {
        qApp->removeTranslator(this);
    }

    bool eventFilter(QObject *watched, QEvent *event)
    {
        if (watched == this && event->type() == QEvent::LanguageChange) {
            load(QLocale(), "ru.auroraos.TrifleVPN", "-", "/usr/share/ru.auroraos.TrifleVPN/translations");
        }

        return QTranslator::eventFilter(watched, event);
    }
};

class TrifleVpnPlugin : public QQmlExtensionPlugin {
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ru.auroraos.TrifleVPNSystemSettings")
public:
    void initializeEngine(QQmlEngine *engine, const char *uri)
    {
        Q_ASSERT(uri == QLatin1String("ru.auroraos.TrifleVPNSystemSettings"));

        // Always load default translations first, otherwise we'll see
        // only string IDs when running jolla-settings from terminal
        TrifleVpnTranslator *defaultTranslator = new TrifleVpnTranslator(engine);
        defaultTranslator->load(QLocale(), "ru.auroraos.TrifleVPN", "-", "/usr/share/ru.auroraos.TrifleVPN/translations");

        TrifleVpnTranslator *translator = new TrifleVpnTranslator(engine);
        translator->load(QLocale(), "ru.auroraos.TrifleVPN", "-", "/usr/share/ru.auroraos.TrifleVPN/translations");
        engine->installEventFilter(translator);
    }

    void registerTypes(const char *uri)
    {
        Q_ASSERT(uri == QLatin1String("ru.auroraos.TrifleVPNSystemSettings"));

        qmlRegisterType<StubItem>(uri, 1, 0, "StubItem");
    }
};

#include "triflevpn_settings_plugin.moc"
