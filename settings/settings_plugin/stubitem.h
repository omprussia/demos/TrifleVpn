// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef STUBITEM_H
#define STUBITEM_H

#include <QObject>

class StubItem : public QObject
{
    Q_OBJECT
public:
    explicit StubItem(QObject *parent = nullptr);
};

#endif // STUBITEM_H
