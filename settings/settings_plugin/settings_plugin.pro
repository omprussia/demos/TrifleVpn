# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
TARGET = ru.auroraos.TrifleVPNPlugin
QT += qml
QT -= gui
CONFIG += plugin c++11

HEADERS += \
    stubitem.h

SOURCES += \
    triflevpn_settings_plugin.cpp \
    stubitem.cpp

OTHER_FILES += qmldir

MODULENAME = ru/auroraos/TrifleVPNSystemSettings
TARGETPATH = $$LIBDIR/qt5/qml/$$MODULENAME

import.files = qmldir
import.path = $$TARGETPATH
target.path = $$TARGETPATH

INSTALLS += target import
