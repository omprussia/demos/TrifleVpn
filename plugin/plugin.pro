# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = lib
TARGET = triflevpn
CONFIG -= qt
CONFIG += no_plugin_name_prefix
CONFIG += plugin c++11
CONFIG += link_pkgconfig
PKGCONFIG += dbus-1 glib-2.0

QMAKE_CXXFLAGS += $$(CXXFLAGS)
QMAKE_CFLAGS += $$(CFLAGS)
QMAKE_LFLAGS += $$(LDFLAGS)

SOURCES += triflevpn_plugin.c

target.path = $$LIBDIR/connman/plugins-vpn/

INSTALLS += target
