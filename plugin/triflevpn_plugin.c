// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#define CONNMAN_API_SUBJECT_TO_CHANGE

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <net/if.h>
#include <sys/un.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <linux/if_tun.h>

#include <glib-2.0/glib.h>
#include <dbus-1.0/dbus/dbus.h>

#include <connman/plugin.h>
#include <connman/task.h>
#include <connman/log.h>
#include <connman/dbus.h>
#include <connman/ipconfig.h>
#include <connman/inet.h>
#include <connman/agent.h>
#include <connman/setting.h>
#include <connman/vpn-dbus.h>
#include <connman/vpn/plugins/vpn.h>
#include <connman/vpn/vpn-agent.h>

#define UNUSED(x) (void)(x)

#define PLUGIN_NAME "triflevpn"
#define BIN_PATH "/usr/libexec/ru.auroraos.TrifleVPN/provider"

#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))

static DBusConnection *connection;

enum opt_type {
    OPT_STRING = 0,
    OPT_BOOL = 1,
};

struct {
    const char *cm_opt;
    const char *tv_opt;
    char has_value;
    enum opt_type opt_type;
} tv_options[] = {
    { "dummyvpn.Port", "--port", 1, OPT_STRING },
    { "dummyvpn.TrustedCert", "--trusted_cert", 1, OPT_STRING },
    { "dummyvpn.BlockIPv6", "--block_ipv6", 0, OPT_BOOL },
    { "dummyvpn.AppendAdditionalGateways", NULL, 0, OPT_BOOL }
};

struct tv_private_data {
    struct vpn_provider *provider;
    struct connman_task *task;
    char *dbus_sender;
    char *if_name;
    vpn_provider_connect_cb_t cb;
    void *user_data;
    char *mgmt_path;
    guint mgmt_timer_id;
    int mgmt_socket_fd;
    guint mgmt_event_id;
    GIOChannel *mgmt_channel;
    int connect_attempts;
    int failed_attempts_privatekey;
};

/* Finalize the connection in any case. By calling the vpn-provider.c callback. */
static void tv_connect_done(struct tv_private_data *data, int err)
{
    if (data && data->cb) {
        vpn_provider_connect_cb_t cb = data->cb;
        void *user_data = data->user_data;

        // Make sure we don't invoke this callback twice
        data->cb = NULL;
        data->user_data = NULL;
        cb(data->provider, user_data, err);
    }

    if (!err)
        data->failed_attempts_privatekey = 0;
}

static void free_private_data(struct tv_private_data *data)
{
    if (vpn_provider_get_plugin_data(data->provider) == data)
        vpn_provider_set_plugin_data(data->provider, NULL);

    tv_connect_done(data, EIO);
    vpn_provider_unref(data->provider);
    g_free(data->dbus_sender);
    g_free(data->if_name);
    g_free(data->mgmt_path);
    g_free(data);
}

/* Add configuration data for task to be used as startup args */
static int task_append_config_data(struct vpn_provider *provider, struct connman_task *task)
{
    bool block_ipv6 = false;
    int i;

    for (i = 0; i < (int)ARRAY_SIZE(tv_options); i++) {
        const char *cm_opt = tv_options[i].cm_opt;
        const char *tv_opt = tv_options[i].tv_opt;
        bool has_value = tv_options[i].has_value;
        const char *option = NULL;

        if (!tv_opt)
            continue;

        switch (tv_options[i].opt_type) {
        case OPT_STRING:
            option = vpn_provider_get_string(provider, cm_opt);
            if (!option)
                continue;

            break;
        case OPT_BOOL:
            if (!vpn_provider_get_boolean(provider, cm_opt, false))
                continue;

            break;
        }

        if (connman_task_add_argument(task, tv_opt, has_value ? option : NULL) < 0)
            return -EIO;
    }

    block_ipv6 = vpn_provider_get_boolean(provider, "dummyvpn.BlockIPv6", false);
    vpn_provider_set_supported_ip_networks(provider, true, !block_ipv6);

    return 0;
}

static void close_management_interface(struct tv_private_data *data)
{
    if (data->mgmt_path) {
        if (unlink(data->mgmt_path) && errno != ENOENT) {
            connman_warn("Unable to unlink management socket %s: %d",
                         data->mgmt_path, errno);
        }

        g_free(data->mgmt_path);
        data->mgmt_path = NULL;
    }

    if (data->mgmt_timer_id != 0) {
        g_source_remove(data->mgmt_timer_id);
        data->mgmt_timer_id = 0;
    }

    if (data->mgmt_event_id) {
        g_source_remove(data->mgmt_event_id);
        data->mgmt_event_id = 0;
    }

    if (data->mgmt_channel) {
        g_io_channel_shutdown(data->mgmt_channel, FALSE, NULL);
        g_io_channel_unref(data->mgmt_channel);
        data->mgmt_channel = NULL;
    }
}

/* Called when VPN goes down, propagate the call to vpn_died(),
 * if this is not implemented vpn_died() is called directly. This allows neat
 * cleanup of any data related to this connection.
 */
static void tv_died(struct connman_task *task, int exit_code, void *user_data)
{
    struct tv_private_data *data = (struct tv_private_data *)user_data;

    //Cancel any pending agent requests
    connman_agent_cancel(data->provider);
    close_management_interface(data);

    vpn_died(task, exit_code, data->provider);
    free_private_data(data);
}

static void return_credentials(struct tv_private_data *data, const char *username, const char *password)
{
    GString *reply_string;
    gchar *reply;
    gsize len;

    reply_string = g_string_new(NULL);

    g_string_append(reply_string, "REPLY:Credentials\n");
    g_string_append(reply_string, username);
    g_string_append_c(reply_string, '\n');

    g_string_append(reply_string, password);
    g_string_append_c(reply_string, '\n');

    len = reply_string->len;
    reply = g_string_free(reply_string, FALSE);

    g_io_channel_write_chars(data->mgmt_channel, reply, len, NULL, NULL);
    g_io_channel_flush(data->mgmt_channel, NULL);

    memset(reply, 0, len);
    g_free(reply);
}

/* Demonstrates the use of password for password protected private key file */
static void return_private_key_password(struct tv_private_data *data, const char *privatekeypass)
{
    UNUSED(privatekeypass);

    GString *reply_string;
    gchar *reply;
    gsize len;

    reply_string = g_string_new(NULL);

    g_string_append(reply_string, "password \"Private Key\" ");
    g_string_append_c(reply_string, '\n');

    len = reply_string->len;
    reply = g_string_free(reply_string, FALSE);

    g_io_channel_write_chars(data->mgmt_channel, reply, len, NULL, NULL);
    g_io_channel_flush(data->mgmt_channel, NULL);

    memset(reply, 0, len);
    g_free(reply);
}

static void request_input_append_informational(DBusMessageIter *iter, void *user_data)
{
    UNUSED(user_data);

    char *str = "string";
    connman_dbus_dict_append_basic(iter, "Type", DBUS_TYPE_STRING, &str);

    str = "informational";
    connman_dbus_dict_append_basic(iter, "Requirement", DBUS_TYPE_STRING, &str);
}

static void request_input_append_mandatory(DBusMessageIter *iter, void *user_data)
{
    UNUSED(user_data);

    char *str = "string";
    connman_dbus_dict_append_basic(iter, "Type", DBUS_TYPE_STRING, &str);

    str = "mandatory";
    connman_dbus_dict_append_basic(iter, "Requirement", DBUS_TYPE_STRING, &str);
}

static void request_input_append_password(DBusMessageIter *iter, void *user_data)
{
    UNUSED(user_data);

    char *str = "password";
    connman_dbus_dict_append_basic(iter, "Type", DBUS_TYPE_STRING, &str);

    str = "mandatory";
    connman_dbus_dict_append_basic(iter, "Requirement", DBUS_TYPE_STRING, &str);
}

/* Example of how to process request input credentials reply */
static void request_input_credentials_reply(DBusMessage *reply, void *user_data)
{
    struct tv_private_data *data = (struct tv_private_data *)user_data;
    char *password = NULL;
    char *username = NULL;
    char *key;
    DBusMessageIter iter, dict;
    DBusError error;
    int err = 0;

    connman_debug("provider %p", data->provider);

    if (!reply) {
        err = ENOENT;
        goto err;
    }

    dbus_error_init(&error);

    /* Check the error with VPN agent function to get proper code and to
     * get callback called.
     */
    err = vpn_agent_check_and_process_reply_error(reply, data->provider, data->task, data->cb, data->user_data);
    if (err) {
        // Ensure cb is called only once
        data->cb = NULL;
        data->user_data = NULL;
        return;
    }

    if (!vpn_agent_check_reply_has_dict(reply)) {
        err = ENOENT;
        goto err;
    }

    dbus_message_iter_init(reply, &iter);
    dbus_message_iter_recurse(&iter, &dict);
    while (dbus_message_iter_get_arg_type(&dict) == DBUS_TYPE_DICT_ENTRY) {
        DBusMessageIter entry, value;

        dbus_message_iter_recurse(&dict, &entry);
        if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_STRING)
            break;

        dbus_message_iter_get_basic(&entry, &key);

        if (g_str_equal(key, "dummyvpn.Password")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_VARIANT)
                break;

            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) != DBUS_TYPE_STRING)
                break;

            dbus_message_iter_get_basic(&value, &password);
            vpn_provider_set_string_hide_value(data->provider, key, password);
        } else if (g_str_equal(key, "dummyvpn.Username")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_VARIANT)
                break;

            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) != DBUS_TYPE_STRING)
                break;

            dbus_message_iter_get_basic(&value, &username);
            vpn_provider_set_string_hide_value(data->provider, key, username);
        }

        dbus_message_iter_next(&dict);
    }

    if (!password || !username) {
        vpn_provider_indicate_error(data->provider, VPN_PROVIDER_ERROR_AUTH_FAILED);
        err = EACCES;
        goto err;
    }

    return_credentials(data, username, password);

    return;

err:
    tv_connect_done(data, err);
}

static int request_credentials_input(struct tv_private_data *data)
{
    struct vpn_provider *provider = data->provider;
    DBusMessage *message;
    const char *path, *agent_sender, *agent_path;
    DBusMessageIter iter;
    DBusMessageIter dict;
    int err;
    void *agent;

    agent = connman_agent_get_info(data->dbus_sender, &agent_sender, &agent_path);
    if (!agent || !agent_path)
        return -ESRCH;

    message = dbus_message_new_method_call(agent_sender, agent_path, VPN_AGENT_INTERFACE, "RequestInput");
    if (!message)
        return -ENOMEM;

    dbus_message_iter_init_append(message, &iter);

    path = vpn_provider_get_path(provider);
    dbus_message_iter_append_basic(&iter, DBUS_TYPE_OBJECT_PATH, &path);

    connman_dbus_dict_open(&iter, &dict);

    if (vpn_provider_get_authentication_errors(provider))
        vpn_agent_append_auth_failure(&dict, provider, NULL);

    /* Request temporary properties to pass on to triflevpn */
    connman_dbus_dict_append_dict(&dict, "dummyvpn.Username", request_input_append_mandatory, NULL);
    connman_dbus_dict_append_dict(&dict, "dummyvpn.Password", request_input_append_password, NULL);

    vpn_agent_append_host_and_name(&dict, provider);

    connman_dbus_dict_close(&iter, &dict);

    err = connman_agent_queue_message(provider, message, connman_timeout_input_request(),
                                      request_input_credentials_reply, data, agent);

    if (err < 0 && err != -EBUSY) {
        connman_debug("error %d sending agent request", err);
        dbus_message_unref(message);
        return err;
    }

    dbus_message_unref(message);

    return -EINPROGRESS;
}

/* Demonstrates the way how to handle the input for protected private key. */
static void request_input_private_key_reply(DBusMessage *reply, void *user_data)
{
    struct tv_private_data *data = (struct tv_private_data *)user_data;
    const char *privatekeypass = NULL;
    const char *key;
    DBusMessageIter iter, dict;
    DBusError error;
    int err = 0;

    connman_debug("provider %p", data->provider);

    if (!reply) {
        err = ENOENT;
        goto err;
    }

    dbus_error_init(&error);

    err = vpn_agent_check_and_process_reply_error(reply, data->provider, data->task, data->cb, data->user_data);
    if (err) {
        /* Ensure cb is called only once */
        data->cb = NULL;
        data->user_data = NULL;
        return;
    }

    if (!vpn_agent_check_reply_has_dict(reply)) {
        err = ENOENT;
        goto err;
    }

    dbus_message_iter_init(reply, &iter);
    dbus_message_iter_recurse(&iter, &dict);
    while (dbus_message_iter_get_arg_type(&dict) == DBUS_TYPE_DICT_ENTRY) {
        DBusMessageIter entry, value;

        dbus_message_iter_recurse(&dict, &entry);
        if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_STRING)
            break;

        dbus_message_iter_get_basic(&entry, &key);
        if (g_str_equal(key, "dummyvpn.PrivateKeyPassword")) {
            dbus_message_iter_next(&entry);
            if (dbus_message_iter_get_arg_type(&entry) != DBUS_TYPE_VARIANT)
                break;

            dbus_message_iter_recurse(&entry, &value);
            if (dbus_message_iter_get_arg_type(&value) != DBUS_TYPE_STRING)
                break;

            dbus_message_iter_get_basic(&value, &privatekeypass);
            vpn_provider_set_string_hide_value(data->provider, key, privatekeypass);
        }

        dbus_message_iter_next(&dict);
    }

    if (!privatekeypass) {
        vpn_provider_indicate_error(data->provider, VPN_PROVIDER_ERROR_AUTH_FAILED);
        err = EACCES;
        goto err;
    }

    return_private_key_password(data, privatekeypass);

    return;

err:
    tv_connect_done(data, err);
}

/* This is for requesting the private key input in addition
 * to the credential request. This will create a separate dialog to UI.
 *
 * Also contains examples how to use the credential storage via VPN agent.
 */
static int request_private_key_input(struct tv_private_data *data)
{
    DBusMessage *message;
    const char *path, *agent_sender, *agent_path;
    const char *privatekeypass;
    DBusMessageIter iter;
    DBusMessageIter dict;
    int err;
    void *agent;

    /* First check if this is the second attempt to get the key within
     * this connection. In such case there has been invalid Private Key
     * Password and it must be reset, and queried from user.
     */
    if (data->failed_attempts_privatekey) {
        vpn_provider_set_string_hide_value(data->provider, "dummyvpn.PrivateKeyPassword", NULL);
    } else {
        /* If the encrypted Private key password is kept in memory and
         * use it first. If authentication fails this is cleared,
         * likewise it is when connman-vpnd is restarted.
         */
        privatekeypass = vpn_provider_get_string(data->provider, "dummyvpn.PrivateKeyPassword");
        if (privatekeypass) {
            return_private_key_password(data, privatekeypass);
            goto out;
        }
    }

    agent = connman_agent_get_info(data->dbus_sender, &agent_sender, &agent_path);
    if (!agent || !agent_path)
        return -ESRCH;

    message = dbus_message_new_method_call(agent_sender, agent_path, VPN_AGENT_INTERFACE, "RequestInput");
    if (!message)
        return -ENOMEM;

    dbus_message_iter_init_append(message, &iter);

    path = vpn_provider_get_path(data->provider);
    dbus_message_iter_append_basic(&iter,DBUS_TYPE_OBJECT_PATH, &path);

    connman_dbus_dict_open(&iter, &dict);
    connman_dbus_dict_append_dict(&dict, "dummyvpn.PrivateKeyPassword", request_input_append_password, NULL);

    vpn_agent_append_host_and_name(&dict, data->provider);

    /* Do not allow to store or retrieve the encrypted Private Key pass */
    vpn_agent_append_allow_credential_storage(&dict, false);
    vpn_agent_append_allow_credential_retrieval(&dict, false);

    /* Indicate to keep credentials, the enc Private Key password should not
     * affect the credential storing.
     */
    vpn_agent_append_keep_credentials(&dict, true);

    connman_dbus_dict_append_dict(&dict, "Enter Private Key password", request_input_append_informational, NULL);

    connman_dbus_dict_close(&iter, &dict);

    err = connman_agent_queue_message(data->provider, message, connman_timeout_input_request(),
                                      request_input_private_key_reply, data, agent);

    if (err < 0 && err != -EBUSY) {
        connman_debug("error %d sending agent request", err);
        dbus_message_unref(message);

        return err;
    }

    dbus_message_unref(message);

out:
    return -EINPROGRESS;
}

/* Configure vpn over management interface */
static int vpn_configure(struct tv_private_data *data)
{
    struct vpn_provider *provider = data->provider;
    GIOChannel *mgmt = data->mgmt_channel;
    const char configure_done[] = "CONFIGURE:Done\n";
    const char configure_failed[]= "CONFIGURE:Failed\n";
    const char *value;
    char *address = NULL, *gateway = NULL, *peer = NULL, *netmask = NULL;
    char *nameservers = NULL;
    int inet_ifindex;
    char *route_idx_end;
    char *str = NULL;
    char *property_start = NULL;
    struct connman_ipaddress *ipaddress;

    while (true) {
        if (g_io_channel_read_line(mgmt, &str, NULL, NULL, NULL) != G_IO_STATUS_NORMAL) {
            connman_error("Failed to read data for configuration");
            goto configure_failed;
        }
        str[strlen(str) - 1] = '\0';

        if (!strcmp(str, "CONFIGURE:End")) {
            g_free(str);
            break;
        }
        if (g_str_has_prefix(str, "INTERNAL_IPV4_")) {
            property_start = str + strlen("INTERNAL_IPV4_");
            address = g_strdup(property_start);
        }
        if (g_str_has_prefix(str, "INTERNAL_NETMASK_")) {
            property_start = str + strlen("INTERNAL_NETMASK_");
            netmask = g_strdup(property_start);
        }
        if (g_str_has_prefix(str, "INTERNAL_NAMESERVERS_")) {
            property_start = str + strlen("INTERNAL_NAMESERVERS_");
            nameservers = g_strdup(property_start);
        }
        if (g_str_has_prefix(str, "INTERNAL_ROUTE_")) {
            property_start = str + strlen("INTERNAL_ROUTE_");

            if (g_str_has_prefix(property_start, "NETWORK_"))
                property_start += strlen("NETWORK_");
            else if (g_str_has_prefix(property_start, "NETMASK_"))
                property_start += strlen("NETMASK_");
            else if (g_str_has_prefix(property_start, "GATEWAY_"))
                property_start += strlen("GATEWAY_");

            g_ascii_strtoull(property_start, &route_idx_end, 10);
            property_start = route_idx_end + 1;

            vpn_provider_append_route(provider, str, property_start);
        }
        if (g_str_has_prefix(str, "INTERNAL_MTU_")) {
            property_start = str + strlen("INTERNAL_MTU_");

            inet_ifindex = connman_inet_ifindex(data->if_name);
            connman_inet_set_mtu(inet_ifindex, strtol(property_start, NULL, 10));
        }

        g_free(str);
    }

    ipaddress = connman_ipaddress_alloc(AF_INET);
    if (!ipaddress) {
        g_free(address);
        g_free(gateway);
        g_free(peer);
        g_free(netmask);

        goto configure_failed;
    }

    value = vpn_provider_get_string(provider, "HostIP");
    if (value) {
        vpn_provider_set_string(provider, "Gateway", value);
        gateway = g_strdup(value);
    }

    connman_ipaddress_set_ipv4(ipaddress, address, netmask, gateway);
    connman_ipaddress_set_p2p(ipaddress, true);
    connman_ipaddress_set_peer(ipaddress, peer);
    vpn_provider_set_nameservers(provider, nameservers);
    vpn_provider_set_ipaddress(provider, ipaddress);

    if (vpn_provider_get_boolean(provider, "dummyvpn.AppendAdditionalGateways", false)) {
        const char *gateways[3] = {
            "7.7.7.7",
            "8.8.8.8",
            NULL,
        };

        vpn_provider_append_gateways(provider, gateways);
    }

    g_free(address);
    g_free(gateway);
    g_free(peer);
    g_free(netmask);
    g_free(nameservers);
    connman_ipaddress_free(ipaddress);

    g_io_channel_write_chars(mgmt, configure_done, sizeof(configure_done)-1, NULL, NULL);
    g_io_channel_flush(mgmt, NULL);

    return 0;

configure_failed:
    g_io_channel_write_chars(mgmt, configure_failed, sizeof(configure_failed)-1, NULL, NULL);
    g_io_channel_flush(mgmt, NULL);

    return -1;
}

/* Save options specific to this VPN only, provider saves provider related */
static int tv_vpn_save(struct vpn_provider *provider, GKeyFile *keyfile)
{
    const char *option;
    int i;

    for (i = 0; i < (int)ARRAY_SIZE(tv_options); i++) {
        if (strncmp(tv_options[i].cm_opt, "dummyvpn.", 9) == 0) {
            option = vpn_provider_get_string(provider, tv_options[i].cm_opt);
            if (!option)
                continue;

            g_key_file_set_string(keyfile, vpn_provider_get_save_group(provider),
                                  tv_options[i].cm_opt, option);
        }
    }

    return 0;
}

static int run_connect(struct tv_private_data *data, vpn_provider_connect_cb_t cb, void *user_data)
{
    UNUSED(cb);
    UNUSED(user_data);

    struct connman_task *task = data->task;
    struct vpn_provider *provider = data->provider;
    const char *option = NULL;
    int err = 0;

#if OS_VERSION_MAJOR == 4 && OS_VERISON_MINOR == 1 || OS_VERSION_MAJOR >= 5
    connman_task_add_variable(task, "DATA_DIR",
                              vpn_provider_get_data_directory(provider));
#endif

    connman_task_add_argument(task, "--syslog", NULL);
    connman_task_add_argument(task, "--ifname", data->if_name);
    connman_task_add_argument(task, "--host", vpn_provider_get_host(provider));
    option = vpn_provider_get_string(provider, "dummyvpn.Port");
    if (option)
        connman_task_add_argument(task, "--port", option);

    connman_task_add_argument(task, "--with_management", NULL);
    connman_task_add_argument(task, "--configure_through_management", NULL);
    connman_task_add_argument(task, "--management_path", data->mgmt_path);
    connman_task_add_argument(task, "--need_notify", NULL);
    connman_task_add_argument(task, "--creds_input_method", "management_interface");

    err = connman_task_run(task, tv_died, data, NULL, NULL, NULL);
    if (err < 0) {
        data->cb = NULL;
        data->user_data = NULL;
        connman_error("triflevpn failed to start");
        return err;
    } else {
        /* This lets the caller know that the actual result of
         * the operation will be reported to the callback
         */
        return -EINPROGRESS;
    }
}

static gboolean tv_vpn_management_handle_input(GIOChannel *source, GIOCondition condition, gpointer user_data)
{
    struct tv_private_data *tv_data = (struct tv_private_data *)user_data;
    char *str = NULL;
    int err = 0;

    if (condition & G_IO_IN) {
        if (g_io_channel_read_line(source, &str, NULL, NULL, NULL) != G_IO_STATUS_NORMAL)
            return G_SOURCE_CONTINUE;

        str[strlen(str) - 1] = '\0';

        if (strcmp(str, "REQUEST:Credentials") == 0) {
            err = request_credentials_input(tv_data);
            if (err != -EINPROGRESS)
                goto close;
        } else if (strcmp(str, "REQUEST:Private_key") == 0) {
            err = request_private_key_input(tv_data);
            if (err != -EINPROGRESS)
                goto close;
        } else if (strcmp(str, "NOTIFY:Verification_failed:credentials") == 0) {
             /* This makes it possible to add error only without
              *sending a state change indication signal to the VPN.
              */
            vpn_provider_add_error(tv_data->provider, VPN_PROVIDER_ERROR_AUTH_FAILED);
        } else if (strcmp(str, "NOTIFY:Verification_failed:private_key") == 0) {
            tv_data->failed_attempts_privatekey++;
        } else if (strcmp(str, "CONFIGURE:Begin") == 0) {
            vpn_configure(tv_data);
        }

        g_free(str);
    } else if (condition & (G_IO_ERR | G_IO_HUP)) {
        connman_warn("Management channel termination");
        goto close;
    }

    return G_SOURCE_CONTINUE;

close:
    close_management_interface(tv_data);

    return G_SOURCE_REMOVE;
}

static int tv_vpn_management_connect_timer_cb(gpointer user_data)
{
    struct tv_private_data *data = (struct tv_private_data *) user_data;
    struct sockaddr_un remote;

    if (data->mgmt_channel)
        goto delete_timer;

    int fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd < 0)
        goto failed_attempt;

    memset(&remote, 0, sizeof(remote));
    remote.sun_family = AF_UNIX;
    g_strlcpy(remote.sun_path, data->mgmt_path, sizeof(remote.sun_path));
    if (connect(fd, (struct sockaddr *)&remote, sizeof(remote)) != 0) {
        close(fd);
        goto failed_attempt;
    }

    data->mgmt_channel = g_io_channel_unix_new(fd);
    data->mgmt_event_id = g_io_add_watch(data->mgmt_channel, G_IO_IN | G_IO_ERR | G_IO_HUP,
                                         tv_vpn_management_handle_input, data);

    connman_warn("Connected management socket");
    goto delete_timer;

failed_attempt:
    data->connect_attempts++;
    if (data->connect_attempts > 30) {
        connman_warn("Unable to connect management socket");

delete_timer:
        data->mgmt_timer_id = 0;
        return G_SOURCE_REMOVE;
    }

    return G_SOURCE_CONTINUE;
}

static int tv_vpn_connect(struct vpn_provider *provider, struct connman_task *task,
                          const char *if_name, vpn_provider_connect_cb_t cb,
                          const char *dbus_sender, void *user_data)
{
    const char *tmpdir;
    struct tv_private_data *data;

    data = g_try_new0(struct tv_private_data, 1);
    if (!data)
        return -ENOMEM;

    vpn_provider_set_plugin_data(provider, data);
    data->provider = vpn_provider_ref(provider);
    data->task = task;
    data->dbus_sender = g_strdup(dbus_sender);
    data->if_name = g_strdup(if_name);
    data->cb = cb;
    data->user_data = user_data;

    /* This demonstrates how it is recommended to setup a management
     * interface for a VPN. It is for reacting to control data coming from
     * the VPN, as well as possibly sending data to the VPN.
     */
    tmpdir = getenv("TMPDIR");
    if (!tmpdir || !*tmpdir)
        tmpdir = "/tmp";

    data->mgmt_path = g_strconcat(tmpdir, "/connman-vpn-management-",
                                  vpn_provider_get_ident(provider), NULL);

    if (unlink(data->mgmt_path) != 0 && errno != ENOENT)
        connman_warn("Unable to unlink management socket %s: %d", data->mgmt_path, errno);

    data->mgmt_timer_id = g_timeout_add(200, tv_vpn_management_connect_timer_cb,data);

    task_append_config_data(provider, task);

    return run_connect(data, cb, user_data);
}

/* As of now something calls vpn_provider_indicate_error() twice resulting in
 * the connection error to be increased at every connection failure by 2. So
 * having 2 as limit here means that 1 error is the actual limit.
 */
#define MAX_CONNECTION_ERRORS 2

/* Connect VPN.
 * Get settings from provider using: vpn_provider_get_string().
 * Add arguments to task with connman_task_add_argument().
 *   if_name      interface to use for connect attempt
 *   cb           connect callback (vpn-provider.c)
 *   dbus_sender  address of the caller
 *   user_data    additional data passed by vpn-provider.c
 */
static int tv_connect(struct vpn_provider *provider, struct connman_task *task, const char *if_name,
                      vpn_provider_connect_cb_t cb, const char *dbus_sender, void *user_data)
{
    int errors;

    errors = vpn_provider_get_connection_errors(provider);
    connman_debug("%d connection errors", errors);

    /* When there are too many connection errors, return -ECANCELED to
     * stop autoconnecting. Error counter is reset when the VPN provider
     * is saved (this plugin has no UI so that cannot be done).
     */
    if (errors >= MAX_CONNECTION_ERRORS)
        return -ECANCELED;

    return tv_vpn_connect(provider, task, if_name, cb, dbus_sender, user_data);
}

/* Handle VPN disconnect.
 *
 * Implementation not madnatory
 */
void tv_disconnect(struct vpn_provider *provider)
{
    connman_info("tv_disconnect");

    if (!provider)
        return;

    connman_agent_cancel(provider);
}

/* Handle to finalize connection process.
 */
static int tv_notify(DBusMessage *msg, struct vpn_provider *provider)
{
    DBusMessageIter iter;
    const char *reason;
    struct tv_private_data *data = (struct tv_private_data *)vpn_provider_get_plugin_data(provider);

    dbus_message_iter_init(msg, &iter);
    dbus_message_iter_get_basic(&iter, &reason);
    dbus_message_iter_next(&iter);

    if (!provider) {
        connman_error("No provider found");
        return VPN_STATE_FAILURE;
    }

    if (strcmp(reason, "up")) {
        tv_connect_done(data, EIO);
        return VPN_STATE_DISCONNECT;
    }

    tv_connect_done(data, 0);
    connman_warn("TrifleVPN connected");
    return VPN_STATE_CONNECT;
}

/* Handle exit/error_code. Return value should be translated to enum
 * vpn_provider_error. This is called by vpn/plugins/vpn.c:vpn_died()
 * and after calling vpn_provider_indicate_error() is called with the
 * return value to record the error, changing the state to
 * VPN_PROVIDER_STATE_FAILURE.
 * 
 * Implementation not madnatory.
 */
static int tv_error_code(struct vpn_provider *provider, int exit_code)
{
    UNUSED(provider);

    connman_info("tv_error_code %d", exit_code);

    switch (exit_code) {
    case 0:
        return VPN_PROVIDER_ERROR_UNKNOWN;
    case 1:
        return VPN_PROVIDER_ERROR_CONNECT_FAILED;
    case 2:
        return VPN_PROVIDER_ERROR_LOGIN_FAILED;
    default:
        return VPN_PROVIDER_ERROR_AUTH_FAILED;
    }
}

/* Save the VPN configuration to a keyfile for this specific VPN type. Provider
 * saves provider related configuration options.
 *
 * Implementation not madnatory if there is no specific options.
 */
static int tv_save(struct vpn_provider *provider, GKeyFile *keyfile)
{
    connman_info("tv_save");
    return tv_vpn_save(provider, keyfile);
}

/* Function for returning correct device flags (IFF_TUN / IFF_TAP)
 * based on the vpn_provider content.
 * Use vpn_provider_get_string(provider, parameter) to get proper parameter.
 *
 * Implementation not madnatory.
 */
static int tv_device_flags(struct vpn_provider *provider)
{
    const char *option;

    option = vpn_provider_get_string(provider, "dummyvpn.DeviceType");
    if (!option)
        return IFF_TUN;

    if (g_str_equal(option, "tap"))
        return IFF_TAP;

    if (!g_str_equal(option, "tun"))
        connman_warn("bad dummyvpn.DeviceType value, fallback to tun");

    return IFF_TUN;
}

/* Function for parsing the enviroment values.
 * If this function is defined it is called by vpn-provider.c:route_env_parse().
 *
 * @provider: vpn_provider structure for this plugin
 * @key: Key to parse
 * @family: Protocol family (AF_INET, AF_INET6)
 * @idx: 
 * @type: type of the provider route, defined as enum vpn_provider_route_type
 *        in connman/vpn/vpn-provider.h. Values: PROVIDER_ROUTE_TYPE_NONE = 0,
 *        PROVIDER_ROUTE_TYPE_MASK = 1, PROVIDER_ROUTE_TYPE_ADDR = 2 and
 *        PROVIDER_ROUTE_TYPE_GW = 3
 *
 * @return: 0 when success
 *
 * Implementation not madnatory.
 */
int tv_route_env_parse(struct vpn_provider *provider, const char *key, int *family,
                       unsigned long *idx, enum vpn_provider_route_type *type)
{
    connman_info("tv_route_env_parse");

    UNUSED(provider);

    char *end;
    const char *start;
    
    if (g_str_has_prefix(key, "INTERNAL_ROUTE_NETWORK_")) {
        start = key + strlen("INTERNAL_ROUTE_NETWORK_");
        *type = VPN_PROVIDER_ROUTE_TYPE_ADDR;
    } else if (g_str_has_prefix(key, "INTERNAL_ROUTE_NETMASK_")) {
        start = key + strlen("INTERNAL_ROUTE_NETWORK_");
        *type = VPN_PROVIDER_ROUTE_TYPE_MASK;
    } else if (g_str_has_prefix(key, "INTERNAL_ROUTE_GATEWAY_")) {
        start = key + strlen("INTERNAL_ROUTE_GATEWAY_");
        *type = VPN_PROVIDER_ROUTE_TYPE_GW;
    } else {
        return -EINVAL;
    }

    *family = AF_INET;
    *idx = g_ascii_strtoull(start, &end, 10);

    return 0;
}

/* VPN driver structure, defined in connman/vpn/plugins/vpn.h */
static struct vpn_driver vpn_driver = {
/*    .flags           = VPN_FLAG_NO_TUN, // predefine flags for plugin */
    .connect         = tv_connect,
    .disconnect      = tv_disconnect,
    .notify          = tv_notify,
    .error_code      = tv_error_code,
    .save            = tv_save,
    .device_flags    = tv_device_flags,
    .route_env_parse = tv_route_env_parse,
};

/* Initialization of the plugin.
 * If connection to dbus is required use connman_dbus_get_connection()
 */
static int triflevpn_init(void)
{
    connman_info("triflevpn_init");

    int rval = 0;
    connection = connman_dbus_get_connection();
    rval = vpn_register(PLUGIN_NAME, &vpn_driver, BIN_PATH);

    return rval;
}

static void triflevpn_exit(void)
{
    connman_info("triflevpn_exit");

    vpn_unregister(PLUGIN_NAME);
    dbus_connection_unref(connection);
}

/* Macro to enable the plugin to be loadable by Connman.
 * From: connman/include/plugin.h:
 * CONNMAN_PLUGIN_DEFINE:
 * @name: plugin name
 * @description: plugin description
 * @version: plugin version string
 * @init: init function called on plugin loading
 * @exit: exit function called on plugin removal
 */
CONNMAN_PLUGIN_DEFINE(triflevpn, "Trifle VPN plugin", CONNMAN_VERSION,
                      CONNMAN_PLUGIN_PRIORITY_DEFAULT, triflevpn_init, triflevpn_exit);
