# TrifleVPN

An example demonstrating the API for developing a VPN application for Aurora OS.

The project is a basic example that allows VPN solution developers to understand how to build their projects.
It shows the basic universal logic without being tied to a specific real provider.

A non-existent provider is used as a VPN provider (the name Trifle can be translated as “toy”).

The server to which the connection is made does not exist, so the result "connected successfully" is immediately returned.
At the same time, you can see and debug how the client part works.

The project contains an example of a toy VPN provider Trifle, a plugin for connman and for system settings (jolla-settings),
as well as a GUI application for communicating with the plugin.

## Installing

For correct working it's nessecary to install two rpm-packages:
* ru.auroraos.TrifleVPN-{version}.{arch}.rpm - the package contains the application, the plugin for settings, the plugin for the Connman.
* ru.auroraos.TrifleVPN-provider-{version}.{arch}.rpm - the package contains the provider called by the plugin for connection.

## VPN provider

It's a binary application installing a VPN connection. It may be run by the user with the settings system menu or by
the user's own desktop application.

The application can configure the network device using the connman plugin.
There is a problem with installing the ru.auroraos.TrifleVPN-provider-{version}.{arch}.rpm package via sdk.
This package can be installed in 2 ways:
+ Using system key signing.
+ Disabling package validation (dev mode required).
  
The package is installed via psdk as standard.

## Plugin to integrate in connman

The plugin defines the necessary behavior to start a VPN tunnel and establishes a communication channel to the tunnel through which TUN/TAP interfaces can be configured.

+ [triflevpn_plugin.c](plugin/triflevpn_plugin.c) — contains the necessary functions to register the plugin in the Connman VPN.

## Plugin to integrate in the system settings

Includes `.qml` files that will be dynamically loaded by system settings:

+ [advanced.qml](settings/settings_controls/triflevpn/advanced.qml)
+ [details.qml](settings/settings_controls/triflevpn/details.qml)
+ [edit.qml](settings/settings_controls/triflevpn/edit.qml)
+ [listitem.qml](settings/settings_controls/triflevpn/listitem.qml)

and plugin `QQmlExtensionPlugin`:

+ [triflevpn_settings_plugin.cpp](settings/settings_plugin/triflevpn_settings_plugin.cpp)

## VPN application

Allows to manage connections related to this VPN.

## IPC between VPN provider and VPN application

The application obtains a socket to communicate with the provider through a call to a DBus service that the provider registers:

+ [Getting the socket](application/src/main.cpp#L53) on the application side.
+ [Service registration](provider/tunnel.c#L572) on the provider side.

In order to the application has access a DBus service that the provider registers, the provider must register a 
service named `ORG_NAME.APP_NAME`, where `APP_NAME` is [the name of the application](application/ru.auroraos.TrifleVPN.desktop#L11).

## Important

To install the application, its RPM package must be signed with **extended** profile key.

## Terms of Use and Participation

The source code of the project is provided under [the license](LICENSE.BSD-3-Clause.md),
which allows its use in third-party applications.

The [contributor agreement](CONTRIBUTING.md) documents the rights granted by contributors
of the Open Mobile Platform.

Information about the contributors is specified in the [AUTHORS](AUTHORS.md) file.

[Code of conduct](CODE_OF_CONDUCT.md) is a current set of rules of the Open Mobile
Platform which informs you how we expect the members of the community will interact
while contributing and communicating.

## Project Structure

* **[application](application)** directory contains implementation of the application to edit the connection settings.
  * **[application.pro](application/application.pro)** file describes the subproject structure for the qmake build system.
  * **[icons](application/icons)** directory contains application icons for different screen resolutions.
  * **[qml](application/qml)** directory contains the QML source code and the UI resources.
    * **[components](application/qml/components)** directory contains user QML components which implements some input fields.
    * **[cover](application/qml/cover)** directory contains the application cover implementations.
    * **[images](application/qml/images)** directory contains the custom images.
    * **[pages](application/qml/pages)** directory contains the application pages.
    * **[TrifleVPN.qml](application/qml/TrifleVPN.qml)** file provides the application window implementation.
  * **[src](application/src)** directory contains the C++ source code.
    * **[main.cpp](application/src/main.cpp)** file is the application entry point.
  * **[ru.auroraos.TrifleVPN.desktop](application/ru.auroraos.TrifleVPN.desktop)** file defines the display and parameters for launching the application.
* **[provider](provider)** directory contains implementation of a VPN provider.
* **[rpm](rpm)** directory contains the rpm-package build settings.
  * **[ru.auroraos.TrifleVPN.spec](rpm/ru.auroraos.TrifleVPN.spec)** file is used by rpmbuild tool.
* **[plugin](plugin)** directory contains the plugin to integrate in connman.
* **[settings](settings)** directory contains the plugin to integrate in the system settings.
* **[translations](translations)** directory contains the UI translation files.
  
## Compatibility

The project is compatible with all the supported versions of the Aurora OS.
Aurora 4 has validator restrictions for installing packages; it is recommended to disable package validation.

## Screenshots

![screenshots](screenshots/screenshots.png)

## This document in Russian / Перевод этого документа на русский язык

- [README.ru.md](README.ru.md)
