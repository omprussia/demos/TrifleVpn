%global __provides_exclude_from ^(/usr/lib/qt5/qml/ru/auroraos/TrifleVPNSystemSettings/|/usr/lib/connman/plugins-vpn/).*$
%global __requires_exclude ^(libru.auroraos.TrifleVPNPlugin|triflvpn)\\.so.*$

Name:       ru.auroraos.TrifleVPN
Summary:    TrifleVPN application, Connman plugin, settings plugin
Version:    0.1
Release:    1
License:    BSD-3-Clause
URL:        https://developer.auroraos.ru/open-source
Source0:    %{name}-%{version}.tar.bz2

Requires:      sailfishsilica-qt5 >= 0.10.9
BuildRequires: pkgconfig(auroraapp)
BuildRequires: pkgconfig(Qt5Core)
BuildRequires: pkgconfig(Qt5Qml)
BuildRequires: pkgconfig(Qt5Quick)
BuildRequires: pkgconfig(Qt5DBus)
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(glib-2.0)
BuildRequires: pkgconfig(gio-unix-2.0)
BuildRequires: connman-devel >= 1.31

%description
%{summary}

%package provider
Summary: TrifleVPN provider

%description provider
%{summary}

%prep
%autosetup

%build
%qmake5
%make_build

%install
%make_install

%files
%defattr(-,root,root,-)
%{_libexecdir}/%{name}/provider
%{_bindir}/%{name}
%defattr(644,root,root,-)
%{_libdir}/connman/plugins-vpn/triflevpn.so
%{_datadir}/sailfish-vpn/triflevpn/
%{_libdir}/qt5/qml/ru/auroraos/TrifleVPNSystemSettings/*
%{_datadir}/%{name}
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.png
