// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef LOG_H
#define LOG_H

#include <stdbool.h>

extern bool use_syslog;

void vpn_log_debug(const char *fmt, ...);
void vpn_log_error(const char *fmt, ...);

#endif // LOG_H
