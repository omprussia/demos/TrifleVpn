// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef TUNNEL_H
#define TUNNEL_H

#include "config.h"

#include <glib-2.0/glib.h>

#define USERNAME_SIZE 64
#define PASSWORD_SIZE 256

struct tunnel;

int run_tunnel(struct vpn_config config, char *username, char *password);

#endif // TUNNEL_H
