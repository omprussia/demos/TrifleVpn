# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = app
TARGET = provider
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += \
    console \
    link_pkgconfig

PKGCONFIG += \
    glib-2.0 \
    gio-unix-2.0

HEADERS +=   \
    config.h \
    log.h    \
    management_interface.h \
    tunnel.h

SOURCES +=   \
    config.c \
    log.c    \
    main.c   \
    management_interface.c \
    tunnel.c

target.path = /usr/libexec/ru.auroraos.TrifleVPN

INSTALLS += target
