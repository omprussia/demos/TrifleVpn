// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "config.h"

#include <string.h>

int
credential_input_method_from_string(char *str)
{
    if (!str)
        return -1;

    if (strcmp(str, "presaved") == 0)
        return (int) PRESAVED;
    else if (strcmp(str, "interactive_cli") == 0)
        return (int) INTERACTIVE_CLI;
    else if (strcmp(str, "management_interface") == 0)
        return (int) MANAGEMENT_INTERFACE;

    return -1;
}
