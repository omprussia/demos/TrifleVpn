// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONFIG_H
#define CONFIG_H

#include <net/if.h>
#include <stdbool.h>

#define HOST_SIZE 16
#define MANAGEMENT_PATH_SIZE 256
#define DATA_DIR_SIZE 256
#define CONNMAN_BUSNAME_SIZE 256
#define CONNMAN_INTERFACE_SIZE 256
#define CONNMAN_PATH_SIZE 256

enum credential_input_method {
    PRESAVED,
    INTERACTIVE_CLI,
    MANAGEMENT_INTERFACE
};

struct vpn_config {
    char if_name[IF_NAMESIZE];
    char host[HOST_SIZE];
    uint port;
    bool with_management;
    char management_path[MANAGEMENT_PATH_SIZE];
    bool need_notify;
    bool configure_through_management;
    enum credential_input_method creds_input_method;
    bool block_ipv6;
    char data_dir[DATA_DIR_SIZE];
    char connman_busname[CONNMAN_BUSNAME_SIZE];
    char connman_interface[CONNMAN_INTERFACE_SIZE];
    char connman_path[CONNMAN_PATH_SIZE];
};

int credential_input_method_from_string(char *str);

#endif // CONFIG_H
