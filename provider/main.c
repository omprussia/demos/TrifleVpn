﻿// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <ctype.h>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>

#include "config.h"
#include "tunnel.h"
#include "log.h"

#define USAGE \
"Usage: dummyvpn []\n" \

bool use_syslog = false;

int
is_digits_only(const char *s)
{
    while (*s) {
        if (isdigit(*s++) == 0) return 0;
    }

    return 1;
}

int
main(int argc, char **argv)
{
    int ret = EXIT_SUCCESS;
    char username[USERNAME_SIZE];
    char password[PASSWORD_SIZE];
    char *data_dir = NULL;
    char *connman_busname = NULL, *connman_interface = NULL, *connman_path = NULL;

    memset(username, 0, USERNAME_SIZE);
    memset(password, 0, PASSWORD_SIZE);

    struct vpn_config config = {
        .if_name = {'\0'},
        .host = {"\0"},
        .port = 10443,
        .with_management = false,
        .management_path = {'\0'},
        .need_notify = false,
        .configure_through_management = false,
        .creds_input_method = INTERACTIVE_CLI,
        .block_ipv6 = false,
        .data_dir = {'\0'},
        .connman_busname = {'\0'},
        .connman_interface = {'\0'},
        .connman_path = {'\0'}
    };

    const struct option long_options[] = {
        {"help",   no_argument, NULL, 0},
        {"version", no_argument, NULL, 0},
        {"ifname", required_argument, NULL, 0},
        {"host", required_argument, NULL, 0},
        {"port", required_argument, NULL, 0},
        {"with_management", no_argument, NULL, 0},
        {"management_path", required_argument, NULL, 0},
        {"need_notify", no_argument, NULL, 0},
        {"configure_through_management", no_argument, NULL, 0},
        {"creds_input_method", required_argument, NULL, 0},
        {"block_ipv6", no_argument, NULL, 0},
        {"username", required_argument, NULL, 'u'},
        {"password", required_argument, NULL, 'p'},
        {"syslog", no_argument, NULL, 0}
    };

    while (1) {
        int c, option_index = 0;

        c = getopt_long(argc, argv, "", long_options, &option_index);
        if (c == -1)
            break;

        switch(c) {
        case 0:
            if (strcmp(long_options[option_index].name, "help") == 0) {
                printf(USAGE);
                ret = EXIT_SUCCESS;
                goto exit;
            }
            if (strcmp(long_options[option_index].name, "version") == 0) {
                printf("0.1.3\n");
                ret = EXIT_SUCCESS;
                goto exit;
            }
            if (strcmp(long_options[option_index].name, "ifname") == 0) {
                strncpy(config.if_name, optarg, IF_NAMESIZE - 1);
                config.if_name[IF_NAMESIZE-1] = '\0';
                break;
            }
            if (strcmp(long_options[option_index].name, "host") == 0) {
                strncpy(config.host, optarg, HOST_SIZE - 1);
                config.host[HOST_SIZE - 1] = '\0';
                break;
            }
            if (strcmp(long_options[option_index].name, "port") == 0) {
                if (!is_digits_only(optarg)) {
                    ret = EXIT_FAILURE;
                    goto user_error;
                }
                config.port = strtol(optarg, NULL, 10);
                break;
            }
            if (strcmp(long_options[option_index].name, "with_management") == 0) {
                config.with_management = true;
                break;
            }
            if (strcmp(long_options[option_index].name, "management_path") == 0) {
                strncpy(config.management_path, optarg, MANAGEMENT_PATH_SIZE - 1);
                config.management_path[MANAGEMENT_PATH_SIZE-1] = '\0';
                break;
            }
            if (strcmp(long_options[option_index].name, "need_notify") == 0) {
                config.need_notify = true;
                break;
            }
            if (strcmp(long_options[option_index].name, "configure_through_management") == 0) {
                config.configure_through_management = true;
                break;
            }
            if (strcmp(long_options[option_index].name, "creds_input_method") == 0) {
                int creds_input_method = credential_input_method_from_string(optarg);
                if (creds_input_method < 0) {
                    goto user_error;
                }
                config.creds_input_method = creds_input_method;
                break;
            }
            if (strcmp(long_options[option_index].name, "block_ipv6") == 0) {
                config.block_ipv6 = true;
                break;
            }
            if (strcmp(long_options[option_index].name, "syslog") == 0) {
                use_syslog = true;
                break;
            }
            goto user_error;
        case 'u':
            strncpy(username, optarg, USERNAME_SIZE);
            username[USERNAME_SIZE-1] = '\0';
            break;
        case 'p':
            strncpy(password, optarg, PASSWORD_SIZE);
            password[PASSWORD_SIZE-1] = '\0';
            break;
        default:
            goto user_error;
        }
    }

    if (config.configure_through_management && !config.with_management) {
        vpn_log_error("Cannot configure throught management interface without --with_management argument");
        ret = EXIT_FAILURE;
        goto exit;
    }

    if (config.need_notify) {
        connman_busname = getenv("CONNMAN_BUSNAME");
        connman_interface = getenv("CONNMAN_INTERFACE");
        connman_path = getenv("CONNMAN_PATH");
        if (!connman_busname || !connman_interface || !connman_path) {
            vpn_log_error("Cannot notify connman without connman dbus env variables");
            ret = EXIT_FAILURE;
            goto exit;
        }
        memset(config.connman_busname, 0, CONNMAN_BUSNAME_SIZE);
        strncpy(config.connman_busname, connman_busname, CONNMAN_BUSNAME_SIZE);
        config.connman_busname[CONNMAN_BUSNAME_SIZE-1] = '\0';

        memset(config.connman_interface, 0, CONNMAN_INTERFACE_SIZE);
        strncpy(config.connman_interface, connman_interface, CONNMAN_INTERFACE_SIZE);
        config.connman_interface[CONNMAN_INTERFACE_SIZE-1] = '\0';

        memset(config.connman_path, 0, CONNMAN_PATH_SIZE);
        strncpy(config.connman_path, connman_path, CONNMAN_PATH_SIZE);
        config.connman_path[CONNMAN_PATH_SIZE-1] = '\0';
    }

    data_dir = getenv("DATA_DIR");
    if (!data_dir) {
        vpn_log_error("There is no DATA_DIR environment variable");
    } else {
        memset(config.data_dir, 0, DATA_DIR_SIZE);
        strncpy(config.data_dir, data_dir, DATA_DIR_SIZE);
        config.data_dir[DATA_DIR_SIZE-1] = '\0';
    }

    if (run_tunnel(config, username, password) != 0)
        ret = EXIT_FAILURE;
    else
        ret = EXIT_SUCCESS;

    goto exit;

user_error:
    vpn_log_error(USAGE);

exit:
    exit(ret);
}
