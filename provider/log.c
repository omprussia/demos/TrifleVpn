// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "log.h"

#include <stdio.h>
#include <syslog.h>
#include <stdarg.h>

static void
vpn_log(int _pri, const char *fmt, va_list vargs)
{
    if (use_syslog) {
        vsyslog(_pri, fmt, vargs);
        return;
    }

    vprintf(fmt, vargs);
}

void
vpn_log_debug(const char *fmt, ...)
{
    va_list vargs;
    va_start(vargs, fmt);

    vpn_log(LOG_DEBUG, fmt, vargs);

    va_end(vargs);
}

void
vpn_log_error(const char *fmt, ...)
{
    va_list vargs;
    va_start(vargs, fmt);

    vpn_log(LOG_ERR, fmt, vargs);

    va_end(vargs);
}
