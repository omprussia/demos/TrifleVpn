// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef MANAGEMENT_INTERFACE_H
#define MANAGEMENT_INTERFACE_H

#include "tunnel.h"

#include <glib-2.0/glib.h>

GIOChannel *management_interface_start(GIOFunc   input_cb,
                                       gpointer  input_cb_user_data,
                                       char     *management_path,
                                       bool      wait_master);
int management_interface_close(GIOChannel *mgmt);

#endif // MANAGEMENT_INTERFACE_H
