// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "management_interface.h"
#include "log.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>

struct management_interface {
    GIOChannel *channel;
    GIOFunc input_cb;
    gpointer input_cb_user_data;
};

int
_handle_new_connection(GIOChannel   *income_channel,
                       GIOCondition condition,
                       gpointer     user_data)
{
    struct management_interface *mgmt = (struct management_interface *) user_data;
    struct sockaddr_storage income;
    int income_socket_fd, conn_fd;
    socklen_t income_len;

    income_socket_fd = g_io_channel_unix_get_fd(income_channel);
    income_len = sizeof(income_socket_fd);

    if (condition & (G_IO_HUP | G_IO_ERR)) {
        vpn_log_error("Income socket closed");
        return G_SOURCE_REMOVE;
    }
    conn_fd = accept(income_socket_fd, (struct sockaddr *)&income, &income_len);
    if (conn_fd == -1) {
        vpn_log_error("Failed to accept management connection");
        return G_SOURCE_CONTINUE;
    }

    mgmt->channel = g_io_channel_unix_new(conn_fd);
    g_io_add_watch(mgmt->channel, G_IO_IN | G_IO_ERR | G_IO_HUP,
                   mgmt->input_cb, (gpointer) mgmt->input_cb_user_data);

    vpn_log_debug("Added new management connection");

    return G_SOURCE_REMOVE;
}

GIOChannel *
management_interface_start(GIOFunc   input_cb,
                           gpointer  input_cb_user_data,
                           char     *management_path,
                           bool      wait_master)
{
    GIOChannel *income_channel;
    struct sockaddr_un addr;
    int fd;
    struct management_interface mgmt = {
        .channel = NULL,
        .input_cb = input_cb,
        .input_cb_user_data = input_cb_user_data
    };

    fd = socket(AF_UNIX, SOCK_STREAM, 0);
    if (fd < 0) {
        vpn_log_error("Failed to open management socket");
        return NULL;
    }

    unlink(management_path);

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    g_strlcpy(addr.sun_path, management_path, sizeof(addr.sun_path));

    if (bind(fd, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
        vpn_log_error("Failed to bind management socket");
        return NULL;
    }
    listen(fd, 2);

    income_channel = g_io_channel_unix_new(fd);

    if (wait_master)
        _handle_new_connection(income_channel, 1, (gpointer)&mgmt);
    else
        g_io_add_watch(income_channel, G_IO_IN | G_IO_ERR,
                       (GIOFunc) _handle_new_connection, (gpointer)&mgmt);

    return mgmt.channel;
}

int
management_interface_close(GIOChannel *mgmt)
{
    if (!mgmt)
        g_io_channel_shutdown(mgmt, true, NULL);

    return 0;
}
