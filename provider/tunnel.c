// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include "tunnel.h"
#include "log.h"
#include "management_interface.h"

#include <errno.h>
#include <fcntl.h>
#include <linux/if_tun.h>
#include <net/if.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>

#include <glib.h>
#include <gio/gio.h>
#include <gio/gunixfdlist.h>

#define REQUEST_CREDENTIALS_INPUT_MSG "REQUEST:Credentials\n"
#define REQUEST_PRIVATE_KEY_INPUT_MSG "REQUEST:Private_key\n"
#define REPLY_CREDENTIALS_INPUT_MSG "REPLY:Credentials\n"
#define REPLY_PRIVATE_KEY_INPUT_MSG "REPLY:Private_key\n"

#define CONFIGURE_BEGIN "CONFIGURE:Begin\n"
#define CONFIGURE_INTERNAL_IPV4 "INTERNAL_IPV4_"
#define CONFIGURE_NETMASK "INTERNAL_NETMASK_"
#define CONFIGURE_NAMESERVERS "INTERNAL_NAMESERVERS_"
#define CONFIGURE_ROUTES "INTERNAL_ROUTE_"
#define CONFIGURE_MTU "INTERNAL_MTU_"
#define CONFIGURE_END "CONFIGURE:End\n"

#define CONFIGURE_DONE "CONFIGURE:Done"

#define IPV4_SIZE 16
#define NETMASK_SIZE 16

#define UNUSED(x) (void)(x)

enum management_msg_code {
    REPLY_CREDENTIALS,
    REPLY_PRIVATE_KEY,
};

struct dv_ipaddress_v4 {
    char ip[IPV4_SIZE];
    char netmask[NETMASK_SIZE];
    char *gateway;
};

static void
dv_ipaddress_v4_set(struct dv_ipaddress_v4 *address,
                    const char             *ip,
                    const char             *netmask,
                    const char             *gateway)
{
    memset(address->ip, 0, IPV4_SIZE);
    memset(address->netmask, 0, NETMASK_SIZE);

    strncpy(address->ip, ip, IPV4_SIZE);
    address->ip[IPV4_SIZE-1] = '\0';
    strncpy(address->netmask, netmask, NETMASK_SIZE);
    address->netmask[NETMASK_SIZE-1] = '\0';
    address->gateway = g_strdup(gateway);
}

struct tunnel {
    struct vpn_config config;
    GIOChannel *mgmt;
    GMainLoop *loop;
    FILE *runtime_data;
    guint service_owner_id;
    GDBusNodeInfo *service_introspection;
    int sv[2];
    int exit_code;
    int mtu;
    struct dv_ipaddress_v4 internal_address;
    char *nameservers;
    struct dv_ipaddress_v4 *routes;
    unsigned int routes_len;
    char username[USERNAME_SIZE];
    char password[PASSWORD_SIZE];
};

static void
tunnel_set_internal_address(struct tunnel *tunnel,
                            const char    *ipv4,
                            const char    *netmask)
{
    dv_ipaddress_v4_set(&tunnel->internal_address, ipv4, netmask, NULL);
}

static void
tunnel_set_credentials(struct tunnel *tunnel,
                       const char    *username,
                       const char    *password)
{
    memset(tunnel->username, 0, USERNAME_SIZE);
    memset(tunnel->password, 0, PASSWORD_SIZE);

    strncpy(tunnel->username, username, USERNAME_SIZE);
    tunnel->username[USERNAME_SIZE-1] = '\0';
    strncpy(tunnel->password, password, PASSWORD_SIZE);
    tunnel->password[PASSWORD_SIZE-1] = '\0';
}

static void
tunnel_clean(struct tunnel *tunnel)
{
    if (tunnel->mgmt) {
        management_interface_close(tunnel->mgmt);
        tunnel->mgmt = NULL;
    }

    g_bus_unown_name(tunnel->service_owner_id);
    g_dbus_node_info_unref(tunnel->service_introspection);

    fflush(tunnel->runtime_data);
    fclose(tunnel->runtime_data);
}

static int
tunnel_configure(struct tunnel *tunnel)
{
    GString *msg_buf = g_string_new(NULL);
    gchar *msg = NULL;
    gsize len;
    GIOChannel *mgmt = tunnel->mgmt;
    unsigned int i;

    if (!mgmt) {
        vpn_log_error("There is no management interface");
        return -1;
    }

    g_string_append_printf(msg_buf, "%s", CONFIGURE_BEGIN);

    g_string_append_printf(msg_buf, "%s%s\n", CONFIGURE_INTERNAL_IPV4,
                           tunnel->internal_address.ip);
    g_string_append_printf(msg_buf, "%s%s\n", CONFIGURE_NETMASK,
                           tunnel->internal_address.netmask);
    g_string_append_printf(msg_buf, "%s%s\n", CONFIGURE_NAMESERVERS, tunnel->nameservers);
    g_string_append_printf(msg_buf, "%s%d\n", CONFIGURE_MTU, tunnel->mtu);

    for (i=0; i<tunnel->routes_len; i++) {
        g_string_append_printf(msg_buf, "%s%s%d_%s\n", CONFIGURE_ROUTES, "NETWORK_", 0,
                               tunnel->routes[i].ip);
        g_string_append_printf(msg_buf, "%s%s%d_%s\n", CONFIGURE_ROUTES, "NETMASK_", 0,
                               tunnel->routes[i].netmask);
        g_string_append_printf(msg_buf, "%s%s%d_%s\n", CONFIGURE_ROUTES, "GATEWAY_", 0,
                               tunnel->routes[i].gateway);
    }

    g_string_append_printf(msg_buf, "%s", CONFIGURE_END);

    len = msg_buf->len;
    msg = g_string_free(msg_buf, FALSE);
    g_io_channel_write_chars(mgmt, msg, len, NULL, NULL);
    g_io_channel_flush(mgmt, NULL);

    g_free(msg);

    if (g_io_channel_read_line(mgmt, &msg, NULL, NULL, NULL) != G_IO_STATUS_NORMAL) {
        vpn_log_error("Failed to read msg from management channel while configurate");
        return -1;
    }
    msg[strlen(msg) - 1] = '\0';

    if (!strcmp(msg, CONFIGURE_DONE))
        return 0;

    return -1;
}

static int
tun_alloc(char *dev,
          int   flags)
{
    struct ifreq ifr;
    int fd, err;

    if ((fd = open("/dev/net/tun", O_RDWR)) < 0) {
        vpn_log_error("Failed to open /dev/net/tun");
        vpn_log_error(strerror(errno));
        return fd;
    }

    memset(&ifr, 0, sizeof(ifr));
    ifr.ifr_flags = flags;

    if (dev && *dev)
        strncpy(ifr.ifr_name, dev, IFNAMSIZ);

    if ((err = ioctl(fd, TUNSETIFF, (void *) &ifr)) < 0) {
        vpn_log_error("Failed to open tun device");
        close(fd);
        return err;
    }

    return fd;
}

static int
handle_mgmt_input(struct tunnel *tunnel,
                  int            msg_code)
{
    GIOChannel *mgmt = tunnel->mgmt;

    switch(msg_code) {
    case REPLY_CREDENTIALS: {
        char *username, *password;
        g_io_channel_read_line(mgmt, &username, NULL, NULL, NULL);
        g_io_channel_read_line(mgmt, &password, NULL, NULL, NULL);
        if (!username || !password) {
            vpn_log_error("Failed to read credentials from management channel");
            return -1;
        }
        tunnel_set_credentials(tunnel, username, password);

        break;
    }
    default:
        vpn_log_error("Unknown reply header");
        return -1;

    }

    return 0;
}

static int
mgmt_msg_code_from_string(char *str)
{
    if (!str)
        return -1;

    if (g_str_has_prefix(str, REPLY_CREDENTIALS_INPUT_MSG))
         return (int) REPLY_CREDENTIALS;

    if (g_str_has_prefix(str, REPLY_PRIVATE_KEY_INPUT_MSG))
        return (int) REPLY_PRIVATE_KEY;

    return -1;
}

static int
mgmt_get_input_msg_code(GIOChannel *mgmt_channel)
{
    char *msg;

    if (g_io_channel_read_line(mgmt_channel, &msg, NULL, NULL, NULL) != G_IO_STATUS_NORMAL) {
        vpn_log_error("Failed to read input from management channel");
        return -1;
    }
    vpn_log_debug("Get management msg header");
    vpn_log_debug(msg);

    return mgmt_msg_code_from_string(msg);
}

static int
mgmt_input_cb(GIOChannel   *mgmt_channel,
              GIOCondition condition,
              gpointer     user_data)
{
    struct tunnel *tunnel = (struct tunnel *) user_data;
    if (condition & G_IO_HUP) {
        vpn_log_debug("Management interface has been closed");
        management_interface_close(mgmt_channel);
        tunnel->mgmt = NULL;
        return G_SOURCE_REMOVE;
    } else if (condition & G_IO_IN) {
        int msg_code = mgmt_get_input_msg_code(mgmt_channel);
        if (msg_code < 0) {
            vpn_log_error("Unknown message");
            return G_SOURCE_CONTINUE;
        }
        handle_mgmt_input(tunnel, msg_code);
    }

    return G_SOURCE_CONTINUE;
}

static int
notify_connman(struct tunnel *tunnel)
{
    int ret;
    const char *busname = tunnel->config.connman_busname;
    const char *interface = tunnel->config.connman_interface;
    const char *path = tunnel->config.connman_path;
    const char *reason = "up";
    GError *error = NULL;
    GDBusConnection *conn;
    GVariant *reply;

    conn = g_bus_get_sync(G_BUS_TYPE_SYSTEM, NULL, NULL);

    reply = g_dbus_connection_call_sync(conn,
                                        busname,
                                        path,
                                        interface,
                                        "notify",
                                        g_variant_new("(s)", reason),
                                        NULL,
                                        G_DBUS_CALL_FLAGS_NONE,
                                        -1,
                                        NULL,
                                        &error);
    if (!reply) {
        vpn_log_error("Failed to send notify message: %s", error->message);
        g_error_free(error);
        ret = -1;
        goto out;
    }

    g_variant_unref(reply);
    ret = 0;

out:
    g_object_unref(conn);
    return ret;
}

static int
request_credentials(struct tunnel *tunnel)
{
    if (!tunnel->mgmt) {
        vpn_log_error("There is no master on management connection");
        return -1;
    }

    GIOChannel *mgmt = tunnel->mgmt;

    g_io_channel_write_chars(mgmt, REQUEST_CREDENTIALS_INPUT_MSG,
                             sizeof(REQUEST_CREDENTIALS_INPUT_MSG), NULL, NULL);
    g_io_channel_flush(mgmt, NULL);

    int reply_msg_code = -1;
    do {
        reply_msg_code = mgmt_get_input_msg_code(mgmt);
    } while (reply_msg_code != REPLY_CREDENTIALS);

    if (handle_mgmt_input(tunnel, reply_msg_code) != 0) {
        vpn_log_error("Failed to get reply for credentials request");
        return -1;
    }

    return 0;
}

static int
input_credentials(struct tunnel *tunnel)
{
    if (*tunnel->username && *tunnel->password)
        return 0;

    /* TODO: parse input from cli */
    return -1;
}

static int
get_credentials(struct tunnel *tunnel)
{
    struct vpn_config *config = &tunnel->config;

    switch (config->creds_input_method) {
    case MANAGEMENT_INTERFACE:
        return request_credentials(tunnel);
    case INTERACTIVE_CLI:
        return input_credentials(tunnel);
    case PRESAVED:
        vpn_log_error("PRESAVE input unimplemented");
        return -1;
    default:
        return -1;
    }
}

static int
process_tun_in(GIOChannel   *tun_channel,
               GIOCondition condition,
               gpointer     user_data )
{
    (void)(user_data);

    gchar buf[1400];
    gsize bytes_read;
    memset(buf, 0, sizeof(buf));

    if (condition & G_IO_IN) {
        g_io_channel_read_chars(tun_channel, buf, sizeof(buf),
                                         &bytes_read, NULL);
    }

    return G_SOURCE_CONTINUE;
}

static FILE *
storage_open(const char *data_dir,
             const char *filename,
             const char *mode)
{
    char *file_path = g_build_filename(data_dir, filename, NULL);

    return fopen(file_path, mode);
}

static int
up_tunnel(struct tunnel *tunnel)
{
    /* Authenticate, set up the tunnel, get local ip,
     * routing tables, nameservers and other things
     * that real vpn provider do but TrifleVPN doesn't.
     */

    struct dv_ipaddress_v4 route;

    tunnel_set_internal_address(tunnel, "10.0.0.50", "255.255.255.0");

    dv_ipaddress_v4_set(&route, "120.0.0.0", "255.255.255.0",
                        tunnel->internal_address.ip);

    tunnel->routes_len = 1;
    tunnel->routes = malloc(sizeof(struct dv_ipaddress_v4) * tunnel->routes_len);
    if (!tunnel->routes) {
        vpn_log_error("No mem");
        return -1;
    }

    tunnel->nameservers = "10.0.15.30 10.0.16.30";
    tunnel->routes[0] = route;

    if (tunnel->runtime_data) {
        const char *runtime_data_to_save = "important data";
        fprintf(tunnel->runtime_data, "%s\n", runtime_data_to_save);
        fflush(tunnel->runtime_data);
    }

    return 0;
}

#define PROVIDER_SERVICE   "ru.auroraos.triflevpn.vpn_provider"
#define PROVIDER_PATH      "/ru/auroraos/triflevpn/vpn_provider"
#define PROVIDER_INTERFACE "ru.auroraos.triflevpn.vpn_provider"

static const gchar provider_introspection_xml[] =
  "<node>"
  "  <interface name='ru.auroraos.triflevpn.vpn_provider'>"
  "    <method name='GetFdForIpc'>"
  "      <arg type='h' name='fd' direction='out'/>"
  "    </method>"
  "  </interface>"
  "</node>";

static void
handle_method_call(GDBusConnection       *connection,
                   const gchar           *sender,
                   const gchar           *object_path,
                   const gchar           *interface_name,
                   const gchar           *method_name,
                   GVariant              *parameters,
                   GDBusMethodInvocation *invocation,
                   gpointer               user_data)
{
    UNUSED(sender);
    UNUSED(object_path);
    UNUSED(interface_name);
    UNUSED(parameters);

    struct tunnel *tunnel = user_data;

    if (g_strcmp0(method_name, "GetFdForIpc") == 0) {
        GDBusMessage *reply;
        GUnixFDList *fd_list;

        fd_list = g_unix_fd_list_new();
        vpn_log_error("append fd");
        g_unix_fd_list_append(fd_list, tunnel->sv[1], NULL);
        close(tunnel->sv[1]);

        reply = g_dbus_message_new_method_reply(g_dbus_method_invocation_get_message(invocation));
        g_dbus_message_set_unix_fd_list(reply, fd_list);

        /* Set index to the file descriptor in fd array.
         * See D-Bus marshalling of the UNIX_FD at D-Bus Specification
         */
        g_dbus_message_set_body(reply, g_variant_new("(h)", 0));

        g_dbus_connection_send_message(connection,
                                       reply,
                                       G_DBUS_SEND_MESSAGE_FLAGS_NONE,
                                       NULL,
                                       NULL);
        g_object_unref(invocation);
        g_object_unref(fd_list);
        g_object_unref(reply);

        const char handshake_msg[] = "hello";
        if (write(tunnel->sv[0], handshake_msg, sizeof(handshake_msg)) == -1)
            vpn_log_error("Failed to send msg via socket");
    }
}

static const GDBusInterfaceVTable provider_interface = {
    handle_method_call,
    { 0 }
};

static void
on_bus_acquired (GDBusConnection *connection,
                 const gchar     *name,
                 gpointer         user_data)
{
    UNUSED(name);

    struct tunnel *tunnel = user_data;
    int registration_id;

    registration_id = g_dbus_connection_register_object(connection,
                                                        PROVIDER_PATH,
                                                        tunnel->service_introspection->interfaces[0],
                                                        &provider_interface,
                                                        tunnel,
                                                        NULL,
                                                        NULL);
    if (registration_id < 0)
        vpn_log_error("Failed to register service interface");
}

static void
on_name_acquired (GDBusConnection *connection,
                  const gchar     *name,
                  gpointer         user_data)
{
    UNUSED(connection);
    UNUSED(name);
    UNUSED(user_data);
}

static void
on_name_lost (GDBusConnection *connection,
              const gchar     *name,
              gpointer         user_data)
{
    UNUSED(connection);
    UNUSED(name);
    UNUSED(user_data);

    vpn_log_error("Service lost it's name");
}

static void
update_dbus_env()
{
    char buf[64];
    char *dbus_session_bus_address = getenv("DBUS_SESSION_BUS_ADDRESS");
    if (!dbus_session_bus_address) {
        snprintf(buf, sizeof(buf), "unix:path=/run/user/%d/dbus/user_bus_socket", getuid());
        setenv("DBUS_SESSION_BUS_ADDRESS", buf, 1);
    }
}

static void
provider_register_service(struct tunnel *tunnel)
{
    update_dbus_env();

    g_assert_nonnull(tunnel);
    tunnel->service_introspection = g_dbus_node_info_new_for_xml(provider_introspection_xml,
                                                                 NULL);

    tunnel->service_owner_id = g_bus_own_name(G_BUS_TYPE_SESSION,
                                              PROVIDER_SERVICE,
                                              G_BUS_NAME_OWNER_FLAGS_NONE,
                                              on_bus_acquired,
                                              on_name_acquired,
                                              on_name_lost,
                                              tunnel,
                                              NULL);
}

int
run_tunnel(struct vpn_config config,
           char              *username,
           char              *password)
{
    GIOChannel *tun_channel;
    int tun_fd;
    int ret;
    struct tunnel tunnel = {
        .config = config,
        .mgmt = NULL,
        .mtu = 1400,
        .loop = NULL,
        .runtime_data = NULL,
        .service_introspection = NULL,
        .sv = {0,0},
        .nameservers = NULL,
        .routes = NULL,
        .routes_len = 0,
        .exit_code = 0
    };  
    bool wait_master = (config.creds_input_method == MANAGEMENT_INTERFACE);

    memset(&tunnel.username, 0, USERNAME_SIZE);
    memset(&tunnel.password, 0, PASSWORD_SIZE);
    tunnel_set_credentials(&tunnel, username, password);

    if (config.with_management) {
        tunnel.mgmt = management_interface_start(mgmt_input_cb, &tunnel,
                                                 config.management_path, wait_master);
        if (!tunnel.mgmt) {
            vpn_log_error("Failed to init management interface");
            goto management_interface_init_failed;
        }
    }

    if ((tun_fd = tun_alloc(config.if_name, IFF_TUN)) < 0) {
        vpn_log_error("Failed to connect to the tun device");
        goto tun_alloc_failed;
    }
    tun_channel = g_io_channel_unix_new(tun_fd);
    g_io_channel_set_encoding(tun_channel, NULL, NULL);
    g_io_channel_set_flags(tun_channel, G_IO_FLAG_NONBLOCK, NULL);
    g_io_add_watch(tun_channel, G_IO_IN | G_IO_ERR | G_IO_HUP,
                   (GIOFunc) process_tun_in, NULL);

    if (get_credentials(&tunnel) != 0) {
        vpn_log_error("Failed to get credentials");
        goto tunnel_up_failed;
    }

    if (*tunnel.config.data_dir) {
        tunnel.runtime_data = storage_open(tunnel.config.data_dir, "runtime_data.txt", "wt");
        if (!tunnel.runtime_data) {
            vpn_log_error("Failed to open storage");
            goto tunnel_up_failed;
        }
    }

    if (up_tunnel(&tunnel) != 0) {
        vpn_log_debug("Failed to up tunnel");
        goto tunnel_up_failed;
    }

    if (config.configure_through_management) {
        if (tunnel_configure(&tunnel) != 0) {
            vpn_log_error("Failed to configure tunnel through management interface");
            goto tunnel_up_failed;
        }
    }

    if (socketpair(AF_UNIX, SOCK_STREAM, 0, tunnel.sv) < 0) {
        vpn_log_error("Failed to create socketpair");
        goto tunnel_up_failed;
    }

    /* Register provider service on session DBus bus.
     * The service send fd from `socketpair` syscall to another vendor application.
     */
    provider_register_service(&tunnel);

    if (config.need_notify) {
        if (notify_connman(&tunnel) != 0) {
            vpn_log_error("Failed to notify connman");
            goto tunnel_up_failed;
        }
    }

    tunnel.loop = g_main_loop_new(NULL, FALSE);
    g_main_loop_run(tunnel.loop);

    ret = tunnel.exit_code;
    tunnel_clean(&tunnel);

    return ret;

tunnel_up_failed:
tun_alloc_failed:
    if (tunnel.mgmt) {
        management_interface_close(tunnel.mgmt);
        tunnel.mgmt = NULL;
    }

management_interface_init_failed:
    return -1;
}
