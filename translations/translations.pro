# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = aux

TRANSLATIONS = \
    ru.auroraos.TrifleVPN.ts \
    ru.auroraos.TrifleVPN-ru.ts

for(t, TRANSLATIONS) {
    TRANSLATIONS_IN += $${_PRO_FILE_PWD_}/$$t
}

qm.files = $$replace(TRANSLATIONS_IN, \.ts, .qm)
qm.path = /usr/share/ru.auroraos.TrifleVPN/translations
qm.CONFIG += no_check_exist
qm.commands += lupdate -no-obsolete -locations none $$PWD/.. -ts $$TRANSLATIONS_IN \
               && lrelease -idbased $$TRANSLATIONS_IN

QMAKE_EXTRA_TARGETS += qm
INSTALLS += qm
PRE_TARGETDEPS += qm

QMAKE_CLEAN += "$$PWD/*.qm"
