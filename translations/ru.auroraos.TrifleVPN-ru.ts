<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name></name>
    <message id="trifle_vpn-server-port">
        <source>Server port</source>
        <translation>Порт сервера</translation>
    </message>
    <message id="trifle_vpn-type-name">
        <source>Trifle type name</source>
        <translation>TrifleVPN</translation>
    </message>
    <message id="trifle_vpn-connection-add">
        <source>Trifle add connection</source>
        <translation>Добавить соединение</translation>
    </message>
    <message id="trifle_vpn-connection-edit">
        <source>Edit</source>
        <translation>Редактировать соединение</translation>
    </message>
    <message id="trifle_vpn-type-description">
        <source>Trifle list item description</source>
        <translation>Пример VPN клиента</translation>
    </message>
    <message id="trifle_vpn-trusted-cert-fingerprint">
        <source>Trusted certificate fingerprint</source>
        <translation>Отпечаток доверенного сертификата</translation>
    </message>
    <message id="trifle_vpn-server-options">
        <source>Server options</source>
        <translation>Настройки сервера</translation>
    </message>
    <message id="trifle_vpn-security-options">
        <source>Security options</source>
        <translation>Настройки безопасности</translation>
    </message>
    <message id="trifle_vpn-security-block-ipv6">
        <source>Disable IPv6</source>
        <translation>Отключить IPv6</translation>
    </message>
    <message id="trifle_vpn-about-application">
        <source>About Application</source>
        <translation>О приложении</translation>
    </message>
    <message id="trifle_vpn-description-text">
        <source>Description Text</source>
        <translation>&lt;p&gt;Short description of my Aurora OS Application&lt;/p&gt;</translation>
    </message>
    <message id="trifle_vpn-3-clause-bsd-license">
        <source>3-Clause BSD Licence</source>
        <translation> Лицензия 3-Clause BSD</translation>
    </message>
    <message id="trifle_vpn-license-text">
        <source>Licence Text</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2024 ru.auroraos&lt;/i&gt;&lt;/p&gt;
        &lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
        &lt;ol&gt;
                &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
                &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
                &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
        &lt;/ol&gt;
        &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
    <message id="trifle_vpn-connection-name">
        <source>Connection name</source>
        <translation>Название соединения</translation>
    </message>
    <message id="trifle_vpn-server-ip">
        <source>Server ip</source>
        <translation>Адрес сервера</translation>
    </message>
    <message id="trifle_vpn-connection-create">
        <source>Create connection</source>
        <translation>Создать соединение</translation>
    </message>
    <message id="trifle_vpn-connection-properties">
        <source>Connection properties</source>
        <translation>Настройки соединения</translation>
    </message>
    <message id="trifle_vpn-connection-delete">
        <source>Delete connection</source>
        <translation>Удалить соединение</translation>
    </message>
    <message id="triflevpn_cover">
        <source>triflevpn</source>
        <translation>TrifleVPN</translation>
    </message>
    <message id="tirfle_vpn-cannot-connect">
        <source>You cannot connect to the VPN from this application</source>
        <translation>Вы не можете подключиться к VPN с помощью этого приложения</translation>
    </message>
    <message id="trifle_vpn-append-additional-gateways">
        <source>Append additional gateways</source>
        <translation>Добавить дополнительные gateways</translation>
    </message>
</context>
</TS>
