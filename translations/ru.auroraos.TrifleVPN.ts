<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en">
<context>
    <name></name>
    <message id="trifle_vpn-server-port">
        <source>Server port</source>
        <translation>Server port</translation>
    </message>
    <message id="trifle_vpn-type-name">
        <source>Trifle type name</source>
        <translation>TrifleVPN</translation>
    </message>
    <message id="trifle_vpn-connection-add">
        <source>Trifle add connection</source>
        <translation>Add connection_dev</translation>
    </message>
    <message id="trifle_vpn-connection-edit">
        <source>Edit</source>
        <translation>Edit connection</translation>
    </message>
    <message id="trifle_vpn-type-description">
        <source>Trifle list item description</source>
        <translation>Trifle VPN client plugin example_dev</translation>
    </message>
    <message id="trifle_vpn-trusted-cert-fingerprint">
        <source>Trusted certificate fingerprint</source>
        <translation>Trusted certificate fingerprint</translation>
    </message>
    <message id="trifle_vpn-server-options">
        <source>Server options</source>
        <translation>Server options</translation>
    </message>
    <message id="trifle_vpn-security-options">
        <source>Security options</source>
        <translation>Security options</translation>
    </message>
    <message id="trifle_vpn-security-block-ipv6">
        <source>Disable IPv6</source>
        <translation>Disable IPv6</translation>
    </message>
    <message id="trifle_vpn-about-application">
        <source>About Application</source>
        <translation>About Application</translation>
    </message>
    <message id="trifle_vpn-description-text">
        <source>Description Text</source>
        <translation>Description Text</translation>
    </message>
    <message id="trifle_vpn-3-clause-bsd-license">
        <source>3-Clause BSD Licence</source>
        <translation>3-Clause BSD Licence</translation>
    </message>
    <message id="trifle_vpn-license-text">
        <source>Licence Text</source>
        <translation>&lt;p&gt;&lt;i&gt;Copyright (C) 2024 ru.auroraos&lt;/i&gt;&lt;/p&gt;
        &lt;p&gt;Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:&lt;/p&gt;
        &lt;ol&gt;
                &lt;li&gt;Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.&lt;/li&gt;
                &lt;li&gt;Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.&lt;/li&gt;
                &lt;li&gt;Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.&lt;/li&gt;
        &lt;/ol&gt;
        &lt;p&gt;THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS &quot;AS IS&quot; AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.&lt;/p&gt;</translation>
    </message>
    <message id="trifle_vpn-connection-name">
        <source>Connection name</source>
        <translation>Connection name</translation>
    </message>
    <message id="trifle_vpn-server-ip">
        <source>Server ip</source>
        <translation>Server ip</translation>
    </message>
    <message id="trifle_vpn-connection-create">
        <source>Create connection</source>
        <translation>Create connection</translation>
    </message>
    <message id="trifle_vpn-connection-properties">
        <source>Connection properties</source>
        <translation>Connection properties</translation>
    </message>
    <message id="trifle_vpn-connection-delete">
        <source>Delete connection</source>
        <translation>Delete connection</translation>
    </message>
    <message id="triflevpn_cover">
        <source>triflevpn</source>
        <translation>TrifleVPN</translation>
    </message>
    <message id="tirfle_vpn-cannot-connect">
        <source>You cannot connect to the VPN from this application</source>
        <translation>You cannot connect to the VPN from this application</translation>
    </message>
    <message id="trifle_vpn-append-additional-gateways">
        <source>Append additional gateways</source>
        <translation>Append additional gateways</translation>
    </message>
</context>
</TS>
