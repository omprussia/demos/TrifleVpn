# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TARGET = ru.auroraos.TrifleVPN

CONFIG += \
    auroraapp \

QT += quick core dbus

SOURCES += \
    src/main.cpp \
    src/marshal_utils.cpp \
    src/triflevpn_connections_filter_model.cpp \
    src/vpn_connection.cpp \
    src/vpn_manager.cpp \
    src/vpn_model.cpp

HEADERS += \
    src/connman_service_interface.h \
    src/connman_vpn_connection_interface.h \
    src/connman_vpn_manager_interface.h \
    src/dbus_types.h \
    src/marshal_utils.h \
    src/triflevpn_connections_filter_model.h \
    src/vpn_connection.h \
    src/vpn_connection_p.h \
    src/vpn_manager.h \
    src/vpn_manager_p.h \
    src/vpn_model.h \
    src/vpn_model_p.h

DISTFILES += \
    AUTHORS.md \
    CODE_OF_CONDUCT.md \
    CONTRIBUTING.md \
    LICENSE.BSD-3-CLAUSE.md \
    README.md \

AURORAAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += auroraapp_i18n
