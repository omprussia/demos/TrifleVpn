// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.TrifleVPN 1.0

import "../components"

Page {
    objectName: "mainPage"
    allowedOrientations: Orientation.All

    PageHeader {
        id: pageHeader

        y: parent.y
        width: parent.width

        //% "Trifle type name"
        title: qsTrId("trifle_vpn-type-name")
        extraContent.children: [
            IconButton {
                objectName: "aboutButton"
                icon.source: "image://theme/icon-m-about"
                anchors.verticalCenter: parent.verticalCenter

                onClicked: pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
            }
        ]
    }

    Button {
        id: newConnectionButton

        width: parent.width
        anchors.top: pageHeader.bottom
        //% "Trifle add connection"
        text: qsTrId("trifle_vpn-connection-add");
        color: "lightblue"

        onClicked: pageStack.push(Qt.resolvedUrl("NewConnection.qml"))
    }

    SilicaListView {
        id: connections
        objectName: "connections"

        anchors.top: newConnectionButton.bottom
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        model: TrifleVpnConnectionsFilterModel {
            sourceModel: VpnModel
        }

        delegate: ConnectionItem {
            connection: model.vpnService
        }
    }

}
