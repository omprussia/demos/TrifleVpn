// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.TrifleVPN 1.0

Page {
    id: newConnecionPage

    SilicaFlickable {
        id: properties

        anchors.fill: parent

        contentHeight: Math.max(propertiesColumn.height, height)

        VerticalScrollDecorator { }

        PageHeader {
            id: pageHeader

            y: parent.y
            width: parent.width

            //% "Connection properties"
            title: qsTrId("trifle_vpn-connection-properties");
        }

        Button {
            id: savePropertiesButton

            width: parent.width
            anchors.top: pageHeader.bottom
            //% "Create connection"
            text: qsTrId("trifle_vpn-connection-create")
            color: "lightblue"

            onClicked: {
                if (!connectionName.text || !serverField.text) {
                    connectionName.errorHighlight = connectionName.text ? false : true
                    serverField.errorHighlight = serverField.text ? false : true
                    return;
                }

                var properties = {
                    name: connectionName.text,
                    host: serverField.text,
                    domain: "omprussia.ru",
                    type: "triflevpn"
                }

                VpnModel.vpnManager.createConnection(properties);
                pageContainer.pop()
            }
        }

        Column {
            id: propertiesColumn

            width: parent.width
            anchors.top: savePropertiesButton.bottom

            TextField {
                id: connectionName

                focus: true
                //% "Connection name"
                label: qsTrId("trifle_vpn-connection-name")
            }

            TextField {
                id: serverField

                //% "Server ip"
                label: qsTrId("trifle_vpn-server-ip")
            }
        }
    }
}
