// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

import QtQuick 2.0
import Sailfish.Silica 1.0
import ru.auroraos.TrifleVPN 1.0

ListItem {
    id: root

    property QtObject connection

    function remove() {
        //% "Delete connection"
        remorseAction(qsTrId("trifle_vpn-connection-delete"),
                      function() { VpnModel.vpnManager.deleteConnection(connection.path)})
    }

    y: parent.y
    width: ListView.view.width
    highlighted: menuOpen

    RemorsePopup {
        id: remorseInfo;
    }

    TextSwitch {
        id: connectionName
        text: connection.name

        onPressAndHold: root.openMenu()
        onClicked: {
            connection.autoConnect = !connection.autoConnect;
            //% "You cannot connect to the VPN from this application"
            remorseInfo.execute(qsTrId("tirfle_vpn-cannot-connect"), function() { })
        }
    }

    openMenuOnPressAndHold: false
    menu: Component {
        ContextMenu {
            MenuItem {
                //% "Edit"
                text: qsTrId("trifle_vpn-connection-edit")
            }
            MenuItem {
                //% "Delete"
                text: qsTrId("trifle_vpn-connection-delete")
                onClicked: remove()
            }
        }
    }
}
