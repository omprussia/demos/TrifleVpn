// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_MODEL_P_H
#define VPN_MODEL_P_H

#include "vpn_model.h"

class VpnModelPrivate
{
    Q_DECLARE_PUBLIC(VpnModel)

public:
    VpnModelPrivate(VpnModel &qq);
    void init();

public:
    VpnManager *m_manager;
    QVector<VpnConnection*> m_connections;
    static const QHash<int, QByteArray> m_roles;

    VpnModel *q_ptr;
};

#endif // VPN_MODEL_P_H
