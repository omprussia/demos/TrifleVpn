// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_MANAGER_H
#define VPN_MANAGER_H

#include <QObject>

class VpnManagerPrivate;
class VpnManager;
class VpnConnection;

class VpnManagerFactory : public QObject
{
    Q_OBJECT

    Q_PROPERTY(VpnManager* instance READ instance CONSTANT)

public:
    static VpnManager* createInstance();
    VpnManager* instance();
};

class VpnManager : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(VpnManager)
    Q_DISABLE_COPY(VpnManager)

    Q_PROPERTY(QVector<VpnConnection*> connections READ connections NOTIFY connectionsChanged)
    Q_PROPERTY(bool populated READ populated NOTIFY populatedChanged)

public:
    explicit VpnManager(QObject *parent = nullptr);
    explicit VpnManager(VpnManagerPrivate &dd, QObject *parent);
    virtual ~VpnManager();

    Q_INVOKABLE void createConnection(const QVariantMap &properties);
    Q_INVOKABLE void modifyConnection(const QString &path, const QVariantMap &properties);
    Q_INVOKABLE void deleteConnection(const QString &path);

    Q_INVOKABLE void activateConnection(const QString &path);
    Q_INVOKABLE void deactivateConnection(const QString &path);

    Q_INVOKABLE VpnConnection *connection(const QString &path) const;
    Q_INVOKABLE VpnConnection *get(int index) const;
    Q_INVOKABLE int indexOf(const QString &path) const;
    int count() const;
    QVector<VpnConnection*> connections() const;
    bool populated() const;

signals:
    void connectionsChanged();
    void connectionAdded(const QString &path);
    void connectionRemoved(const QString &path);
    void connectionsRefreshed();
    void connectionsCleared();
    void populatedChanged();

private:
    QScopedPointer<VpnManagerPrivate> d_ptr;
};

#endif // VPN_MANAGER_H
