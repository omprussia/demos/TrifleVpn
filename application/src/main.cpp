// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <stdio.h>
#include <syslog.h>
#include <unistd.h>

#include <auroraapp.h>
#include <QVariant>
#include <QtQuick>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDBusUnixFileDescriptor>

#include "vpn_manager.h"
#include "vpn_connection.h"
#include "vpn_model.h"
#include "triflevpn_connections_filter_model.h"

const auto provider_service = QStringLiteral("ru.auroraos.triflevpn.vpn_provider");
const auto provider_path = QStringLiteral("/ru/auroraos/triflevpn/vpn_provider");
const auto provider_interface = QStringLiteral("ru.auroraos.triflevpn.vpn_provider");

template<typename T>
static QObject *singleton_api_factory(QQmlEngine *, QJSEngine *)
{
    return new T;
}

int main(int argc, char *argv[])
{
    QScopedPointer<QGuiApplication> application(Aurora::Application::application(argc, argv));
    application->setOrganizationName(QStringLiteral("ru.auroraos"));
    application->setApplicationName(QStringLiteral("TrifleVPN"));

    qmlRegisterSingletonType<VpnManager>("ru.auroraos.TrifleVPN", 1, 0, "VpnManager", singleton_api_factory<VpnManager>);
    qmlRegisterType<VpnConnection>("ru.auroraos.TrifleVPN", 1, 0, "VpnConnection");
    qmlRegisterSingletonType<VpnModel>("ru.auroraos.TrifleVPN", 1, 0, "VpnModel", singleton_api_factory<VpnModel>);
    qmlRegisterType<TrifleVpnConnectionsFilterModel>("ru.auroraos.TrifleVPN", 1, 0, "TrifleVpnConnectionsFilterModel");

    QTranslator appTranslator;
    if (appTranslator.load(QLocale(), "ru.auroraos.TrifleVPN", "-", "/usr/share/ru.auroraos.TrifleVPN/translations"))
        application->installTranslator(&appTranslator);

    QScopedPointer<QQuickView> view(Aurora::Application::createView());
    view->setSource(Aurora::Application::pathTo(QStringLiteral("qml/TrifleVPN.qml")));
    view->show();

    /* Connect to UNIX socket listened by VPN provider
     * when connection state changed to READY
     */  
    auto get_fd_for_ipc = [&application]() {
        VpnManager *vpnManager = VpnManagerFactory::createInstance();
        if (!vpnManager->populated()) {
            syslog(LOG_ERR, "VpnManager didn't populated");
            return;
        }

        auto connections = vpnManager->connections();
        for (auto it = connections.begin(); it != connections.end(); it++) {
            auto connection = *it;
            if (connection->type() != TRIFLE_VPN_TYPE)
                continue;

            auto connect_to_vpn_provider = [connection]() {
                if (connection->state() != VpnConnection::Ready)
                    return;

                auto bus = QDBusConnection::sessionBus();
                if (!bus.isConnected()) {
                    syslog(LOG_ERR, "Cannot connect to the DBus session bus");
                    return;
                }

                /* There is restriction about service name.
                 * Service that vpn provider register must have name starts with pattern
                 * ORG_NAME.APP_NAME where ORG_NAME and APP_NAME taken from the .dekstop
                 * file of the client application.
                 *
                 * In this particular example ORG_NAME=ru.auroraos and APP_NAME=triflevpn.
                 * So the name of the service must starts with `ru.auroraos.triflevpn`.
                 */
                QDBusInterface provider(provider_service, provider_path, provider_interface);
                if (!provider.isValid()) {
                    syslog(LOG_ERR, "Cannot create proxy to the provider service: %s",
                           qPrintable(bus.lastError().message()));
                    return;
                }

                QDBusReply<QDBusUnixFileDescriptor> reply = provider.call("GetFdForIpc");
                if (!reply.isValid()) {
                    syslog(LOG_ERR, "Cannot get FD for IPC: %s",
                           qPrintable(reply.error().message()));
                    return;
                }

                /* Getting unix socket for IPC */
                auto fd = reply.value();
                char *handshake_msg = (char *) malloc(128);
                if (read(fd.fileDescriptor(), handshake_msg, 128) == -1) {
                    syslog(LOG_ERR, "Cannot read handshake message");
                    return;
                }
                syslog(LOG_INFO, "Handshake: %s", handshake_msg);
            };

            application->connect(connection,
                                 &VpnConnection::stateChanged,
                                 connect_to_vpn_provider);
        }
    };

    VpnManager *vpnManager = VpnManagerFactory::createInstance();
    if (vpnManager->populated()) {
        get_fd_for_ipc();
    } else {
        application->connect(vpnManager,
                             &VpnManager::populatedChanged,
                             get_fd_for_ipc);
    }

    return application->exec();
}
