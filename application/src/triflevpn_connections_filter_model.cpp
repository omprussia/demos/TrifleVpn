// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#include <QDebug>

#include "vpn_connection.h"
#include "vpn_model.h"
#include "triflevpn_connections_filter_model.h"

TrifleVpnConnectionsFilterModel::TrifleVpnConnectionsFilterModel(QObject *parent)
    : QSortFilterProxyModel(parent)
{
}

int TrifleVpnConnectionsFilterModel::count() const
{
    return rowCount();
}

bool TrifleVpnConnectionsFilterModel::filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const
{
    if (sourceModel() == nullptr)
        return false;

    QModelIndex index = sourceModel()->index(sourceRow, 0, sourceParent);
    VpnConnection *connection = sourceModel()->data(index, VpnModel::ItemRoles::VpnRole).value<VpnConnection *>();

    return connection->type() == TRIFLE_VPN_TYPE;
}
