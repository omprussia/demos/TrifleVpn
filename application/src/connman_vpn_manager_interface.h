// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONNMAN_VPN_MANAGER_INTERFACE_H
#define CONNMAN_VPN_MANAGER_INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

#include "dbus_types.h"

/* Proxy class for interface net.connman.vpn.Manager
 */
class NetConnmanVpnManagerInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.connman.vpn.Manager"; }

public:
    NetConnmanVpnManagerInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0)
        : QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
    {};

    ~NetConnmanVpnManagerInterface()
    {};

public Q_SLOTS:
    inline QDBusPendingReply<QDBusObjectPath> Create(const QVariantMap &properties)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(properties);
        return asyncCallWithArgumentList(QStringLiteral("Create"), argumentList);
    }

    inline QDBusPendingReply<PathPropertiesArray> GetConnections()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("GetConnections"), argumentList);
    }

    inline QDBusPendingReply<> RegisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QStringLiteral("RegisterAgent"), argumentList);
    }

    inline QDBusPendingReply<> Remove(const QDBusObjectPath &identifier)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(identifier);
        return asyncCallWithArgumentList(QStringLiteral("Remove"), argumentList);
    }

    inline QDBusPendingReply<> UnregisterAgent(const QDBusObjectPath &path)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(path);
        return asyncCallWithArgumentList(QStringLiteral("UnregisterAgent"), argumentList);
    }

Q_SIGNALS:
    void ConnectionAdded(const QDBusObjectPath &identifier, const QVariantMap &properties);
    void ConnectionRemoved(const QDBusObjectPath &identifier);
};

namespace net {
  namespace connman {
    namespace vpn {
      typedef ::NetConnmanVpnManagerInterface Manager;
    }
  }
}
#endif
