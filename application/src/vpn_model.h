// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_MODEL_H
#define VPN_MODEL_H

#include <QAbstractListModel>

class VpnModelPrivate;
class VpnManager;
class VpnConnection;

/* VpnModel is a basic list model for connman VPN services.
 */
class VpnModel : public QAbstractListModel
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(VpnModel)
    Q_DISABLE_COPY(VpnModel)

    Q_PROPERTY(bool connected READ isConnected NOTIFY connectedChanged)
    Q_PROPERTY(int count READ count NOTIFY countChanged)
    Q_PROPERTY(VpnManager *vpnManager READ vpnManager NOTIFY vpnManagerChanged)
    Q_PROPERTY(bool populated READ populated NOTIFY populatedChanged)

public:
    enum ItemRoles {
        VpnRole = Qt::UserRole + 1
    };

    explicit VpnModel(QObject *parent = nullptr);
    explicit VpnModel(VpnModelPrivate &dd, QObject *parent);
    ~VpnModel() Q_DECL_OVERRIDE;

    QVariant data(const QModelIndex &index, int role) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column, const QModelIndex &parent) const Q_DECL_OVERRIDE;
    int count() const;
    bool isConnected() const;
    VpnManager *vpnManager() const;
    bool populated() const;
    Q_INVOKABLE QVariantMap connectionSettings(const QString &path) const;

Q_SIGNALS:
    void connectedChanged(const bool &connected);
    void countChanged();
    void vpnManagerChanged();
    void populatedChanged();

protected Q_SLOTS:
    void connectionsChanged();
    void connectionDestroyed(QObject *);

protected:
    virtual void orderConnections(QVector<VpnConnection*> &connections);
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;
    void moveItem(int oldIndex, int newIndex);
    QVector<VpnConnection*> connections() const;

private:
    QScopedPointer<VpnModelPrivate> d_ptr;
};

#endif // VPN_MODEL_H
