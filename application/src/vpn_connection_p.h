// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_CONNECTION_P_H
#define VPN_CONNECTION_P_H

#include "connman_vpn_connection_interface.h"
#include "connman_service_interface.h"
#include "vpn_connection.h"

class VpnConnectionPrivate
{
    Q_DECLARE_PUBLIC(VpnConnection)

public:
    VpnConnectionPrivate(VpnConnection &qq, const QString &path);
    void init();
    void setProperty(const QString &key, const QVariant &value, void(VpnConnection::*changedSignal)());
    void checkChanged(QVariantMap &properties, QQueue<void(VpnConnection::*)()> &emissions, const QString &name, void(VpnConnection::*changedSignal)());
    template<typename T>
    void updateVariable(QVariantMap &properties, QQueue<void(VpnConnection::*)()> &emissions, const QString &name, T *property, void(VpnConnection::*changedSignal)());

public:
    NetConnmanVpnConnectionInterface m_connectionProxy;
    NetConnmanServiceInterface m_serviceProxy;
    QString m_path;
    bool m_autoConnect;
    bool m_splitRouting;
    bool m_globalStorage;
    VpnConnection::ConnectionState m_state;
    QVariantMap m_properties;

    VpnConnection *q_ptr;
};

#endif // VPN_CONNECTION_P_H
