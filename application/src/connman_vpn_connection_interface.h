// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef CONNMAN_VPN_CONNECTION_INTERFACE_H
#define CONNMAN_VPN_CONNECTION_INTERFACE_H

#include <QtCore/QObject>
#include <QtCore/QByteArray>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtDBus/QtDBus>

#include "dbus_types.h"

/* Proxy class for interface net.connman.vpn.Connection
 */
class NetConnmanVpnConnectionInterface: public QDBusAbstractInterface
{
    Q_OBJECT
public:
    static inline const char *staticInterfaceName()
    { return "net.connman.vpn.Connection"; }

public:
    NetConnmanVpnConnectionInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent = 0)
        : QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
    {}

    ~NetConnmanVpnConnectionInterface()
    {}

public Q_SLOTS:
    inline QDBusPendingReply<> ClearProperty(const QString &name)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(name);
        return asyncCallWithArgumentList(QStringLiteral("ClearProperty"), argumentList);
    }

    inline QDBusPendingReply<> Connect()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("Connect"), argumentList);
    }

    inline QDBusPendingReply<> Connect2(const QString &dbus_sender)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(dbus_sender);
        return asyncCallWithArgumentList(QStringLiteral("Connect2"), argumentList);
    }

    inline QDBusPendingReply<> Disconnect()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("Disconnect"), argumentList);
    }

    inline QDBusPendingReply<QVariantMap> GetProperties()
    {
        QList<QVariant> argumentList;
        return asyncCallWithArgumentList(QStringLiteral("GetProperties"), argumentList);
    }

    inline QDBusPendingReply<> SetProperty(const QString &name, const QDBusVariant &value)
    {
        QList<QVariant> argumentList;
        argumentList << QVariant::fromValue(name) << QVariant::fromValue(value);
        return asyncCallWithArgumentList(QStringLiteral("SetProperty"), argumentList);
    }

Q_SIGNALS:
    void PropertyChanged(const QString &name, const QDBusVariant &value);
};

namespace net {
  namespace connman {
    namespace vpn {
      typedef ::NetConnmanVpnConnectionInterface Connection;
    }
  }
}
#endif
