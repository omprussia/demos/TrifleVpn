// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef MARSHAL_UTILS_H
#define MARSHAL_UTILS_H

#include <QVariant>
#include <QDBusArgument>

namespace MarshalUtils
{
    template <typename Argument>
    inline Argument demarshallArgument(const QVariant &argument)
    {
        if (argument.userType() == qMetaTypeId<QDBusArgument>()) {
            Argument demarshalled;
            argument.value<QDBusArgument>() >> demarshalled;
            return demarshalled;
        } else {
            return argument.value<Argument>();
        }

    }

    typedef QVariant (*conversionFunction)(const QString &key, const QVariant &value, bool toDBus);

    QVariantMap propertiesToQml(const QVariantMap &fromDBus);
    QHash<QString, conversionFunction> propertyConversions();
    QVariant convertValue(const QString &key, const QVariant &value, bool toDBus);
    QVariant convertToQml(const QString &key, const QVariant &value);
    QVariant convertToDBus(const QString &key, const QVariant &value);
    QVariantMap propertiesToDBus(const QVariantMap &fromQml);
}

#endif // MARSHAL_UTILS_H
