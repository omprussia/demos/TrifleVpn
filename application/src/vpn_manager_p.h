// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_MANAGER_P_H
#define VPN_MANAGER_P_H

#include "connman_vpn_manager_interface.h"
#include "vpn_manager.h"

class VpnManagerPrivate : public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(VpnManager)

public:
    VpnManagerPrivate(VpnManager &qq);
    void init();
    void fetchVpnList();
    void setPopulated(bool populated);

    static VpnManagerPrivate *get(VpnManager *manager) { return manager->d_func(); }
    static const VpnManagerPrivate *get(const VpnManager *manager) { return manager->d_func(); }

Q_SIGNALS:
    void beginConnectionsReset();
    void endConnectionsReset();

public:
    NetConnmanVpnManagerInterface m_connmanVpn;
    QVector<VpnConnection*> m_items;
    bool m_populated;

    VpnManager *q_ptr;
};

#endif // VPN_MANAGER_P_H
