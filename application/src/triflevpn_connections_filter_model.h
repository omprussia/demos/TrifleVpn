// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef TRIFLEVPN_CONNECTIONS_FILTER_MODEL_H
#define TRIFLEVPN_CONNECTIONS_FILTER_MODEL_H

#include <QSortFilterProxyModel>
#include <QQmlApplicationEngine>

const auto TRIFLE_VPN_TYPE = QStringLiteral("triflevpn");

class TrifleVpnConnectionsFilterModel : public QSortFilterProxyModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ count NOTIFY countChanged)
public:
    explicit TrifleVpnConnectionsFilterModel(QObject *parent = nullptr);

    int count() const;

signals:
    void countChanged();

protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const override;
};

#endif // TRIFLEVPN_CONNECTIONS_FILTER_MODEL_H
