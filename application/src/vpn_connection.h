// SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
// SPDX-License-Identifier: BSD-3-Clause

#ifndef VPN_CONNECTION_H
#define VPN_CONNECTION_H

#include <QObject>
#include <QVariantMap>

class VpnConnectionPrivate;

/* The userRoutes and serverRoutes properties are QVariants containing a
 * QList<RouteStructure> structure
 */
struct RouteStructure
{
    int protocolFamily;
    QString network;
    QString netmask;
    QString gateway;
};
Q_DECLARE_METATYPE(RouteStructure)

class VpnConnection : public QObject
{
    Q_OBJECT
    Q_DECLARE_PRIVATE(VpnConnection)
    Q_DISABLE_COPY(VpnConnection)

    Q_PROPERTY(QString path READ path CONSTANT)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY nameChanged)
    Q_PROPERTY(QString host READ host WRITE setHost NOTIFY hostChanged)
    Q_PROPERTY(QString domain READ domain WRITE setDomain NOTIFY domainChanged)
    Q_PROPERTY(bool autoConnect READ autoConnect WRITE setAutoConnect NOTIFY autoConnectChanged)
    Q_PROPERTY(bool storeCredentials READ storeCredentials WRITE setStoreCredentials NOTIFY storeCredentialsChanged)
    Q_PROPERTY(ConnectionState state READ state NOTIFY stateChanged)

    Q_PROPERTY(QString type READ type WRITE setType NOTIFY typeChanged)
    Q_PROPERTY(bool immutable READ immutable WRITE setImmutable NOTIFY immutableChanged)
    Q_PROPERTY(int index READ index WRITE setIndex NOTIFY indexChanged)
    Q_PROPERTY(QVariantMap ipv4 READ ipv4 WRITE setIpv4 NOTIFY ipv4Changed)
    Q_PROPERTY(QVariantMap ipv6 READ ipv6 WRITE setIpv6 NOTIFY ipv6Changed)
    Q_PROPERTY(QStringList nameservers READ nameservers WRITE setNameservers NOTIFY nameserversChanged)
    Q_PROPERTY(QVariant userRoutes READ userRoutes WRITE setUserRoutes NOTIFY userRoutesChanged)
    Q_PROPERTY(QVariant serverRoutes READ serverRoutes WRITE setServerRoutes NOTIFY serverRoutesChanged)
    Q_PROPERTY(bool splitRouting READ splitRouting WRITE setSplitRouting NOTIFY splitRoutingChanged)
    Q_PROPERTY(bool globalStorage READ globalStorage WRITE setGlobalStorage NOTIFY globalStorageChanged)

    Q_PROPERTY(QVariantMap properties READ properties WRITE setProperties NOTIFY propertiesChanged)
    Q_PROPERTY(QVariantMap providerProperties READ providerProperties WRITE setProviderProperties NOTIFY providerPropertiesChanged)
    Q_PROPERTY(bool connected READ connected NOTIFY connectedChanged)

public:
    enum ConnectionState {
        Idle,
        Failure,
        Configuration,
        Ready,
        Disconnect
    };
    Q_ENUM(ConnectionState)

    explicit VpnConnection(QObject *parent = nullptr);
    explicit VpnConnection(const QString &path, QObject *parent = nullptr);
    explicit VpnConnection(VpnConnectionPrivate &dd, QObject *parent);
    virtual ~VpnConnection();

    void modifyConnection(const QVariantMap &properties);
    void activate();
    void deactivate();
    void update(const QVariantMap &updateProperties);
    int connected() const;

    QString path() const;

    QString name() const;
    void setName(const QString &name);

    QString host() const;
    void setHost(const QString &host);

    QString domain() const;
    void setDomain(const QString &domain);

    bool autoConnect() const;
    void setAutoConnect(bool autoConnect);

    bool storeCredentials() const;
    void setStoreCredentials(bool storeCredentials);

    ConnectionState state() const;

    QString type() const;
    void setType(const QString &type);

    bool immutable() const;
    void setImmutable(bool immutable);

    int index() const;
    void setIndex(int index);

    QVariantMap ipv4() const;
    void setIpv4(const QVariantMap &iPv4);

    QVariantMap ipv6() const;
    void setIpv6(const QVariantMap &iPv6);

    QStringList nameservers() const;
    void setNameservers(const QStringList &nameservers);

    QVariant userRoutes() const;
    void setUserRoutes(const QVariant &userRoutes);

    QVariant serverRoutes() const;
    void setServerRoutes(const QVariant &serverRoutes);

    bool splitRouting() const;
    void setSplitRouting(bool splitRouting);

    bool globalStorage() const;
    void setGlobalStorage(bool global);

    QVariantMap properties() const;
    void setProperties(const QVariantMap properties);

    QVariantMap providerProperties() const;
    void setProviderProperties(const QVariantMap &providerProperties);

signals:
    void nameChanged();
    void hostChanged();
    void domainChanged();
    void autoConnectChanged();
    void storeCredentialsChanged();
    void stateChanged();
    void typeChanged();
    void immutableChanged();
    void indexChanged();
    void ipv4Changed();
    void ipv6Changed();
    void nameserversChanged();
    void userRoutesChanged();
    void serverRoutesChanged();
    void splitRoutingChanged();
    void globalStorageChanged();
    void propertiesChanged();
    void providerPropertiesChanged();
    void connectedChanged();

private:
    QScopedPointer<VpnConnectionPrivate> d_ptr;
};

#endif // VPN_CONNECTION_H
