# Authors

* Maksim Fedyarov, <m.fedyarov@omp.ru>
  * Reviewer, 2024
  * Developer, 2023
* Kirill Chuvilin, <k.chuvilin@omp.ru>
  * Product owner, 2023
* Konstantin Zvyagin, <k.zvyagin@omp.ru>
  * Reviewer, 2024
* Vladislav Larionov
  * Reviewer, 2023
* Andrej Begichev, <a.begichev@omp.ru>
  * Maintainer, 2024
  * Developer, 2024
* Oksana Torosyan, <o.torosyan@omp.ru>
  * Designer, 2024
