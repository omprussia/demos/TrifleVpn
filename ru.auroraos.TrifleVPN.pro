# SPDX-FileCopyrightText: 2024 Open Mobile Platform LLC <community@omp.ru>
# SPDX-License-Identifier: BSD-3-Clause

TEMPLATE = subdirs

SUBDIRS += \
    plugin \
    application \
    provider \
    translations \
    settings \

DISTFILES += \
    *.md \
    rpm/ru.auroraos.TrifleVPN.spec \
    *.sh
